<?php
namespace Oda;

require '../../../../../header.php';
require '../../../../../vendor/autoload.php';
require '../../../../../config/config.php';

use 
    stdClass,
    Oda\SimpleObject\OdaConfig,
    Aws\S3\S3Client
;

// script/getResources.php?fic=img/no_avatar.png

$config = SimpleObject\OdaConfig::getInstance();

if(isset($config->scriptAvailable) && !$config->scriptAvailable){
    die("service not available");
}

if(isset($_GET["fic"])){
    if (strpos($config->resourcesPath, 's3://') !== false) {
        /** FOR S3 */
    
        //FOR AWS
        if(property_exists($config, 'AWS')){
            $s3 = new S3Client([
                'version' => 'latest',
                'region'  => $config->AWS->region
            ]);
    
            $s3->registerStreamWrapper();
        }

        $path = $config->resourcesPath;
    }else{
        $path = __DIR__;
        $path = str_replace("vendor".DIRECTORY_SEPARATOR."atbq".DIRECTORY_SEPARATOR."oda".DIRECTORY_SEPARATOR."resources".DIRECTORY_SEPARATOR."script", $config->resourcesPath, $path);
    }

    $file = $path . $_GET["fic"];

    if(file_exists($file)){
        $fileName = basename($file);

        switch(strrchr(basename($fileName), ".")){
            case ".gz": $type = "application/x-gzip"; break;
            case ".tgz": $type = "application/x-gzip"; break;
            case ".zip": $type = "application/zip"; break;
            case ".pdf": $type = "application/pdf"; break;
            case ".png": $type = "image/png"; break;
            case ".gif": $type = "image/gif"; break;
            case ".jpg": $type = "image/jpeg"; break;
            case ".txt": $type = "text/plain"; break;
            case ".htm": $type = "text/html"; break;
            case ".html": $type = "text/html"; break;
            default: $type = "application/octet-stream"; break;
        }

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Headers: X-Requested-With');
        header('Access-Control-Allow-Headers: Content-Type');
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT');
        header('Access-Control-Max-Age: 86400');
        header("Content-disposition: inline; filename=$fileName");

        if(isset($_GET["dl"]) && ($_GET["dl"] == "true")){
            header("Content-Type: application/force-download");
            header("Content-Transfer-Encoding: $type\n"); // ne pas enlever le \n
        }else{
            header("Content-Type: $type\n"); // ne pas enlever le \n
        }

        header("Content-Length: ".filesize($file));
        header("Pragma: no-cache");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
        header("Expires: 0");
        readfile($file);
    }else{
        header("HTTP/1.0 400 File not exist");
    }
}