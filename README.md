# ODA_FW_SERVER
Server for the full framework Oda

Basically is of create a REST web server from DB (mysql is better) 

# Installation
1 - get dependency "composer require atbq/oda"

2 - "npm install"

3 - "php vendor/atbq/oda/deploy/installer.php"

4 - Edit "config/config.php"

# For more
Visit: https://oda.myatbq.com