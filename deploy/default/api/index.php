<?php

namespace Project;

require '../header.php';
require "../vendor/autoload.php";
require '../config/config.php';

use 
    Slim\Slim,
    Oda\OdaRestInterface,
    cebe\markdown\GithubMarkdown,
    Oda\SimpleObject\OdaPrepareInterface
;

$slim = new Slim();
//--------------------------------------------------------------------------

$slim->notFound(function () use ($slim) {
    $params = new OdaPrepareInterface($slim);
    $INTERFACE = new OdaRestInterface($params);
    $INTERFACE->dieInError('not found');
});

$slim->get('/', function () {
    $markdown = file_get_contents('./doc.markdown', true);
    $parser = new GithubMarkdown();
    echo $parser->parse($markdown);
});

$slim->get('/entity/:id', function ($id) use ($slim) {
    $params = new OdaPrepareInterface($slim);
    $INTERFACE = new EntityInterface($params);
    $INTERFACE->get($id);
});


$slim->run();