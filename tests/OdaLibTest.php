<?php
/**
 * Date: 26/04/2016
 */

require('vendor/autoload.php');

use 
    Oda\OdaLib,
    PHPUnit\Framework\TestCase
;

class OdaLibTest extends TestCase {
    public function testGetStringBetween() {
        $v_test = OdaLib::get_string_between("01234", "1", "3");

        $this->assertEquals("2", $v_test);
    }

    public function testCryptODA() {
        $v_test = OdaLib::cryptODA(array(
            'userCode' => 'ILRO',
            'valideDate' => 1491902766000
        ));
        $this->assertEquals('7b2275736572436f6465223a22494c524f222c2276616c69646544617465223a313439313930323736363030307d', $v_test);

        $v_test = OdaLib::cryptODA(array(
            'text' => 'Cecé un tes@te.com - / ""$ % 2 à #"ù*!!;'
        ));
        $this->assertEquals('7b2274657874223a224365635c753030653920756e207465734074652e636f6d202d205c2f205c225c222420252032205c753030653020235c225c75303066392a21213b227d', $v_test);

        $v_test = OdaLib::cryptODA(array(
            'text' => "C'est le test de l'apostrophe"
        ));
        $this->assertEquals('7b2274657874223a224327657374206c652074657374206465206c2761706f7374726f706865227d', $v_test);

        $v_test = OdaLib::cryptODA(array(
            'text' => "C&rsquo;est le test de l&rsquo;apostrophe"
        ));
        $this->assertEquals('7b2274657874223a224326727371756f3b657374206c652074657374206465206c26727371756f3b61706f7374726f706865227d', $v_test);
    }

    public function testUncryptODA() {
        $v_test = OdaLib::uncryptODA('7b2275736572436f6465223a22494c524f222c2276616c69646544617465223a313439313930323736363030307d');
        $this->assertEquals(array(
            'userCode' => 'ILRO',
            'valideDate' => 1491902766000
        ), $v_test);

        $v_test = OdaLib::uncryptODA('7b2274657874223a224365635c753030653920756e207465734074652e636f6d202d205c2f205c225c222420252032205c753030653020235c225c75303066392a21213b227d');
        $this->assertEquals(array(
            'text' => 'Cecé un tes@te.com - / ""$ % 2 à #"ù*!!;'
        ), $v_test);

        $v_test = OdaLib::uncryptODA('7b27746f273a273939272c276d657373616765273a27426f6e6a6f75722c3c62723e4e6f75732076656e6f6e7320646520636c6f747572657220766f7472652064656d616e6465206e756de9726f2033302e203c62723e4d6572636920646520766f74726520636f6e6669616e63652c3c62723e4c6520737570706f72742053696c7665722d436c7562272c2766726f6d273a312c276b6579417574684f4441273a276266336436653339363831303364323265656232306332343366396633353565277d');
        $this->assertEquals(array(
            'to' => '99',
            'message' => 'Bonjour,<br>Nous venons de cloturer votre demande numéro 30. <br>Merci de votre confiance,<br>Le support Silver-Club',
            'from' => 1,
            'keyAuthODA' => 'bf3d6e3968103d22eeb20c243f9f355e'
        ), $v_test);

        $v_test = OdaLib::uncryptODA('7b2274657874223a224327657374206c652074657374206465206c2761706f7374726f706865227d');
        $this->assertEquals(array(
            'text' => "C'est le test de l'apostrophe"
        ), $v_test);

        $v_test = OdaLib::uncryptODA('7b2274657874223a224326727371756f3b657374206c652074657374206465206c26727371756f3b61706f7374726f706865227d');
        $this->assertEquals(array(
            'text' => "C&rsquo;est le test de l&rsquo;apostrophe"
        ), $v_test);
    }

    public function testIsTrue() {
        $this->assertEquals(true, OdaLib::isTrue(true));
        $this->assertEquals(true, OdaLib::isTrue("true"));
        $this->assertEquals(true, OdaLib::isTrue(1), true);
        $this->assertEquals(true, OdaLib::isTrue("1"));
        $this->assertEquals(false, OdaLib::isTrue(1.0));
        $this->assertEquals(false, OdaLib::isTrue(false));
        $this->assertEquals(false, OdaLib::isTrue("false"));
        $this->assertEquals(false, OdaLib::isTrue(0));
        $this->assertEquals(false, OdaLib::isTrue("0"));
        $this->assertEquals(false, OdaLib::isTrue(2));
        $this->assertEquals(false, OdaLib::isTrue("2"));
        $this->assertEquals(false, OdaLib::isTrue("hello"));
    }
}