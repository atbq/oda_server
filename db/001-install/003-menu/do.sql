-- --------------------------------------------------------
--
-- Structure de la table `api_tab_menu_category`
-- Reserve 1-9 API
-- Reserve 10-19 API_RH
-- Reserve 70-79 Projet
-- Reserve 98 Liens cachés
-- Reserve 99 Liens externes
--
CREATE TABLE IF NOT EXISTS `@prefix@api_tab_menu_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
);

--
-- Datas
--
INSERT INTO `@prefix@api_tab_menu_category` (`id`, `description`) VALUES
  (1, 'oda-menu-cate.home'),
  (2, 'oda-menu-cate.admin'),
  (3, 'oda-menu-cate.manage'),
  (4, 'oda-menu-cate.reports'),
  (98, 'oda-menu-cate.hiddenLInks'),
  (99, 'oda-menu-cate.extLink');

--
-- Reserve
--
ALTER TABLE `@prefix@api_tab_menu_category` AUTO_INCREMENT = 70;

-- --------------------------------------------------------
--
-- Structure de la table `api_tab_menu`
-- Réserve un plage de tag pour le système
-- Réservé 1-19 API
-- Réservé 20-29 API_RH
-- Réservé : 70-89 Projet
-- Réservé : 100+ pour le projet
--
CREATE TABLE IF NOT EXISTS `@prefix@api_tab_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL,
  `description` varchar(250) NOT NULL,
  `category_id` int(5) NOT NULL,
  `route` text NOT NULL,
  PRIMARY KEY (`id`)
);

--
-- Datas
--
INSERT INTO `@prefix@api_tab_menu` (`label`, `description`, `category_id`, `route`) VALUES
  ('oda-menu.home', 'oda-menu.home', 1, 'home'),
  ('oda-menu.contact', 'oda-menu.contact', 1, 'contact'),
  ('oda-menu.profile', 'oda-menu.profile', 1, 'profile'),
  ('oda-menu.admin', 'oda-menu.admin', 2, 'admin'),
  ('oda-menu.supervision', 'oda-menu.supervision', 2, 'supervision'),
  ('oda-menu.tests', 'oda-menu.tests', 2, 'tests'),
  ('oda-menu.navigation', 'oda-menu.navigation', 2, 'navigation'),
  ('oda-menu.stats', 'oda-menu.stats', 3, 'stats')
;

--
-- Reserve
--
ALTER TABLE `@prefix@api_tab_menu` AUTO_INCREMENT = 100;

-- --------------------------------------------------------
--
-- Structure de la table `api_tab_menu_rangs_droit`
--
CREATE TABLE IF NOT EXISTS `@prefix@api_tab_menu_rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank_id` int(5) NOT NULL,
  `menu_ids` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
);

--
-- Datas
--
INSERT INTO `@prefix@api_tab_menu_rank` (`rank_id`, `menu_ids`) VALUES
  (2, ';'),
  (3, ';'),
  (4, ';'),
  (5, ';')
;

-- --------------------------------------------------------
--
-- Contraites
--
ALTER TABLE  `@prefix@api_tab_menu` ADD CONSTRAINT `api_tab_menu-fk-category_id` FOREIGN KEY ( `category_id` ) REFERENCES `@prefix@api_tab_menu_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;

ALTER TABLE  `@prefix@api_tab_menu_rank` ADD CONSTRAINT `api_tab_menu_rank-fk-rank_id` FOREIGN KEY ( `rank_id` ) REFERENCES `@prefix@api_tab_rank` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;
