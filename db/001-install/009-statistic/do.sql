-- --------------------------------------------------------
--
-- Structure de la table `tab_statistiques_site`
--
CREATE TABLE IF NOT EXISTS `@prefix@api_tab_statistic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL DEFAULT 'route',
  `context` varchar(250) NOT NULL,
  `action` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
);

-- --------------------------------------------------------
--
-- Contraites
--
ALTER TABLE `@prefix@api_tab_statistic` ADD CONSTRAINT `api_tab_statistic-user_id` FOREIGN KEY ( `user_id` ) REFERENCES `@prefix@api_tab_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
