-- --------------------------------------------------------
--
-- Structure de la table `tab_service_mail_dest`
--
CREATE TABLE `@prefix@api_tab_service_mail` (
  id int(11) NOT NULL AUTO_INCREMENT,
  label varchar(100) NOT NULL,
  PRIMARY KEY (id)
);

-- --------------------------------------------------------
--
-- Structure de la table `tab_service_mail_dest`
--
CREATE TABLE `@prefix@api_tab_service_mail_dest` (
  id int(11) NOT NULL AUTO_INCREMENT,
  mail_id int(11) NOT NULL,
  user_code varchar(50) NOT NULL,
  level varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
);

-- --------------------------------------------------------
--
-- Contraites
--
ALTER TABLE `@prefix@api_tab_service_mail_dest` ADD CONSTRAINT `api_tab_service_mail_dest-fk-mail_id` FOREIGN KEY ( `mail_id` ) REFERENCES `@prefix@api_tab_service_mail` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `@prefix@api_tab_service_mail_dest` ADD CONSTRAINT `api_tab_service_mail_dest-fk-user_id` FOREIGN KEY ( `user_code` ) REFERENCES `@prefix@api_tab_user` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION;
