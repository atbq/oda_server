-- --------------------------------------------------------
--
-- Structure de la table `tab_rangs`
--
CREATE TABLE IF NOT EXISTS `@prefix@api_tab_rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(250) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

--
-- Datas
--
INSERT INTO `@prefix@api_tab_rank` (`id`, `label`, `value`) VALUES
  (1, 'oda-rank.admin', 1),
  (2, 'oda-rank.supervisor', 10),
  (3, 'oda-rank.responsible', 20),
  (4, 'oda-rank.user', 30),
  (5, 'oda-rank.visitor', 99)
;

--
-- Reserve
--
ALTER TABLE `@prefix@api_tab_rank` AUTO_INCREMENT = 20;

-- --------------------------------------------------------
--
-- Structure de la table `tab_utilisateurs`
--
CREATE TABLE IF NOT EXISTS `@prefix@api_tab_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `password` varchar(60) NOT NULL,
  `name_first` varchar(20) NOT NULL,
  `name_last` varchar(20) NOT NULL,
  `rank_id` int(4) NOT NULL DEFAULT '5',
  `description` varchar(250) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `active` int(2) NOT NULL DEFAULT '1',
  `creation` datetime NOT NULL,
  `modification` datetime,
  `theme` varchar(50),
  `language` varchar(50) NOT NULL DEFAULT 'fr',
  PRIMARY KEY (`id`),
  UNIQUE `code_unique` (  `code` )
);

--
-- Datas
-- ADMI : pass
-- VIS : VIS
--
INSERT INTO `@prefix@api_tab_user` (`code`, `password`, `name_first`, `name_last`, `rank_id`, `mail`, `creation`, `description`) VALUES
  ('ADMI', '$2y$10$co5O0nZScrI0GJ/HnGD.q.M7dGBtDxGeQHqewXJ9GvO8w.K5ot9mi', 'Administrateur', 'Administrateur', 1, 'admin@mail.com', NOW(), 'for administrator account'),
  ('VIS', '$2y$10$Y.WdJ4dihlbb/ENMOo6MnuFHqQJ2lJ.fZ2kX1jhlhhzx4XtAMBTzm', 'Visiteur', 'Visiteur', 5, 'vis@mail.com', NOW(), 'for visitor account')
;

-- --------------------------------------------------------
--
-- Contraites
--
ALTER TABLE  `@prefix@api_tab_user` ADD CONSTRAINT `api_tab_user-fk-rank_id` FOREIGN KEY ( `rank_id` ) REFERENCES `@prefix@api_tab_rank` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;