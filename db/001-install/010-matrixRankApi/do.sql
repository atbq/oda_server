CREATE TABLE IF NOT EXISTS `@prefix@api_tab_api_rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interface` varchar(255) NOT NULL,
  `method` varchar(255) NOT NULL,
  `rank_id` int(11) NOT NULL,
  `open` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE  `@prefix@api_tab_api_rank` ADD CONSTRAINT `api_tab_api_rank-fk-rank_id` FOREIGN KEY ( `rank_id` ) REFERENCES `@prefix@api_tab_rank` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;

--
-- Datas
--
INSERT INTO `@prefix@api_tab_api_rank` (`interface`, `rank_id`, `open`)
  SELECT 'user',  `id` , FALSE
  FROM `@prefix@api_tab_rank`
  WHERE 1=1
        AND `value` = 20
;