-- --------------------------------------------------------
--
-- Structure de la table `api_tab_message`
--

CREATE TABLE IF NOT EXISTS `@prefix@api_tab_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(2) NOT NULL,
  `message` varchar(500) NOT NULL,
  `rank_id` int(11) NOT NULL,
  `level` varchar(100) NOT NULL,
  `expiration` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `creation` datetime NOT NULL,
  PRIMARY KEY (`id`)
);

--
-- Structure de la table `api_tab_message_read`
--

CREATE TABLE IF NOT EXISTS `@prefix@api_tab_message_read` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `message_id` int(11) NOT NULL,
  `moment` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
);

-- --------------------------------------------------------
--
-- Contraites
--
ALTER TABLE `@prefix@api_tab_message` ADD CONSTRAINT `api_tab_message-fk-user_id` FOREIGN KEY ( `user_id` ) REFERENCES `@prefix@api_tab_user` ( `id` ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `@prefix@api_tab_message` ADD CONSTRAINT `api_tab_message-fk-rank_id` FOREIGN KEY ( `rank_id` ) REFERENCES `@prefix@api_tab_rank` ( `id` ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `@prefix@api_tab_message_read` ADD CONSTRAINT `api_tab_message_read-fk-rmessage_id` FOREIGN KEY ( `message_id` ) REFERENCES  `@prefix@api_tab_message` ( `id` ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `@prefix@api_tab_message_read` ADD CONSTRAINT `api_tab_message_read-fk-rank_id` FOREIGN KEY ( `user_id` ) REFERENCES  `@prefix@api_tab_user` ( `id` ) ON DELETE NO ACTION ON UPDATE NO ACTION;