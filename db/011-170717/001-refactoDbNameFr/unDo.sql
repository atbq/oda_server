SET FOREIGN_KEY_CHECKS=0;
-----------------------------------------------------------
ALTER TABLE `@prefix@api_tab_user` ADD `montrer_aide_ihm` INT(2) NOT NULL DEFAULT '1' AFTER `active`;
RENAME TABLE `@prefix@api_tab_user` TO `@prefix@api_tab_utilisateurs`;
ALTER TABLE `api_tab_utilisateurs` 
    CHANGE `code` `code_user` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `name_last` `nom` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `name_first` `prenom` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `rank_id` `id_rang` INT(4) NOT NULL, 
    CHANGE `active` `actif` INT(2) NOT NULL DEFAULT '1', 
    CHANGE `creation` `date_creation` DATETIME NOT NULL, 
    CHANGE `modification``date_modif`  DATETIME NOT NULL, 
    CHANGE `language` `langue` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'fr'
;
-----------------------------------------------------------
RENAME TABLE `@prefix@api_tab_rank` TO `@prefix@api_tab_rangs`;
ALTER TABLE `@prefix@api_tab_rangs` 
    CHANGE `label` `labelle` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `value` `indice` INT(11) NOT NULL
;
-----------------------------------------------------------
RENAME TABLE `@prefix@api_tab_api_rank` TO `@prefix@api_tab_rang_api`;
ALTER TABLE `@prefix@api_tab_rang_api` 
    CHANGE `method` `methode` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `rank_id` `id_rang` INT(11) NOT NULL
;
-----------------------------------------------------------
ALTER TABLE `@prefix@api_tab_log` 
    CHANGE `type_id` `id_type` INT(4) NOT NULL, 
    CHANGE `message` `commentaires` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
;
-----------------------------------------------------------
ALTER TABLE `@prefix@api_tab_menu` 
    CHANGE `description``Description`  VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `label` `Description_courte` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `category_id` `id_categorie` INT(5) NOT NULL, 
    CHANGE `route` `Lien` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
-----------------------------------------------------------
RENAME TABLE `@prefix@api_tab_menu_category` TO `@prefix@api_tab_menu_categorie`;
ALTER TABLE `@prefix@api_tab_menu_categorie` 
    CHANGE `description` `Description` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
;
-----------------------------------------------------------
RENAME TABLE `@prefix@api_tab_menu_rank` TO `@prefix@api_tab_menu_rangs_droit`;
ALTER TABLE `@prefix@api_tab_menu_rangs_droit` 
    CHANGE `rank_id` `id_rang` INT(5) NOT NULL, 
    CHANGE `menu_ids` `id_menu` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
;
-----------------------------------------------------------
RENAME TABLE `@prefix@api_tab_message` TO `@prefix@api_tab_messages`;
ALTER TABLE `@prefix@api_tab_messages` 
    CHANGE `active` `actif` TINYINT(2) NOT NULL, 
    CHANGE `rank_id` `id_rang` INT(10) NOT NULL, 
    CHANGE `level` `niveau` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `expiration` `date_expiration` DATE NOT NULL, 
    CHANGE `user_id` `id_user` INT(10) NOT NULL, 
    CHANGE `creation` `date_creation` DATETIME NOT NULL
;
-----------------------------------------------------------
RENAME TABLE `@prefix@api_tab_message_read` TO `@prefix@api_tab_messages_lus`;
ALTER TABLE `@prefix@api_tab_messages_lus` 
    CHANGE `user_id` `id_user` INT(10) NOT NULL, 
    CHANGE `message_id` `id_message` INT(11) NOT NULL, 
    CHANGE `moment` `datelu` DATETIME NOT NULL;
-----------------------------------------------------------
ALTER TABLE `@prefix@api_tab_migration` CHANGE `execution` `dateMigration` DATETIME NOT NULL;
-----------------------------------------------------------
RENAME TABLE `@prefix@api_tab_parameter` TO `@prefix@api_tab_parametres`;
ALTER TABLE `@prefix@api_tab_parametres` 
    CHANGE `name` `param_name` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `type` `param_type` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `value` `param_value` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
;
-----------------------------------------------------------
ALTER TABLE `@prefix@api_tab_service_mail` CHANGE `label` `libelle` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
-----------------------------------------------------------
ALTER TABLE `@prefix@api_tab_service_mail_dest` 
    CHANGE `mail_id` `id_type_mail` INT(11) NOT NULL,
    CHANGE `user_code` `code_user` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `level` `nivo` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
;
-----------------------------------------------------------
ALTER TABLE `@prefix@api_tab_session` 
    CHANGE `creation` `dateCreation` DATETIME NOT NULL, 
    CHANGE `ttl` `periodeValideMinute` INT(11) NOT NULL
;
-----------------------------------------------------------
RENAME TABLE `@prefix@api_tab_statistic` TO `@prefix@api_tab_statistiques_site`;
ALTER TABLE `@prefix@api_tab_statistiques_site` DROP `type`;
ALTER TABLE `@prefix@api_tab_statistiques_site` 
    CHANGE `user_id` `id_user` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `context` `page` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
;
-----------------------------------------------------------
ALTER TABLE `@prefix@api_tab_transaction` 
    CHANGE `status``statut`  VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `start` `debut` DATETIME NOT NULL, 
    CHANGE `end` `fin` DATETIME NOT NULL
;
-----------------------------------------------------------
SET FOREIGN_KEY_CHECKS=1;