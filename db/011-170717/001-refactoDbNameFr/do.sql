SET FOREIGN_KEY_CHECKS=0;
-----------------------------------------------------------
ALTER TABLE `@prefix@api_tab_utilisateurs` DROP `montrer_aide_ihm`;
RENAME TABLE `@prefix@api_tab_utilisateurs` TO `@prefix@api_tab_user`;
ALTER TABLE `api_tab_user` 
    CHANGE `code_user` `code` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `nom` `name_last` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `prenom` `name_first` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `id_rang` `rank_id` INT(4) NOT NULL, 
    CHANGE `actif` `active` INT(2) NOT NULL DEFAULT '1', 
    CHANGE `date_creation` `creation` DATETIME NOT NULL, 
    CHANGE `date_modif` `modification` DATETIME NOT NULL, 
    CHANGE `langue` `language` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'fr'
;
-----------------------------------------------------------
RENAME TABLE `@prefix@api_tab_rangs` TO `@prefix@api_tab_rank`;
ALTER TABLE `@prefix@api_tab_rank` 
    CHANGE `labelle` `label` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `indice` `value` INT(11) NOT NULL
;
-----------------------------------------------------------
RENAME TABLE `@prefix@api_tab_rang_api` TO `@prefix@api_tab_api_rank`;
ALTER TABLE `@prefix@api_tab_api_rank` 
    CHANGE `methode` `method` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `id_rang` `rank_id` INT(11) NOT NULL
;
-----------------------------------------------------------
ALTER TABLE `@prefix@api_tab_log` 
    CHANGE `id_type` `type_id` INT(4) NOT NULL, 
    CHANGE `dateTime` `moment` DATETIME NOT NULL,
    CHANGE `commentaires` `message` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
;
-----------------------------------------------------------
ALTER TABLE `@prefix@api_tab_menu` 
    CHANGE `Description` `description` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `Description_courte` `label` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `id_categorie` `category_id` INT(5) NOT NULL, 
    CHANGE `Lien` `route` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
-----------------------------------------------------------
RENAME TABLE `@prefix@api_tab_menu_categorie` TO `@prefix@api_tab_menu_category`;
ALTER TABLE `@prefix@api_tab_menu_category` 
    CHANGE `Description` `description` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
;
-----------------------------------------------------------
RENAME TABLE `@prefix@api_tab_menu_rangs_droit` TO `@prefix@api_tab_menu_rank`;
ALTER TABLE `@prefix@api_tab_menu_rank` 
    CHANGE `id_rang` `rank_id` INT(5) NOT NULL, 
    CHANGE `id_menu` `menu_ids` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
;
-----------------------------------------------------------
RENAME TABLE `@prefix@api_tab_messages` TO `@prefix@api_tab_message`;
ALTER TABLE `@prefix@api_tab_message` 
    CHANGE `actif` `active` TINYINT(2) NOT NULL, 
    CHANGE `id_rang` `rank_id` INT(10) NOT NULL, 
    CHANGE `niveau` `level` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `date_expiration` `expiration` DATE NOT NULL, 
    CHANGE `id_user` `user_id` INT(10) NOT NULL, 
    CHANGE `date_creation` `creation` DATETIME NOT NULL
;
-----------------------------------------------------------
RENAME TABLE `@prefix@api_tab_messages_lus` TO `@prefix@api_tab_message_read`;
ALTER TABLE `@prefix@api_tab_message_read` 
    CHANGE `id_user` `user_id` INT(10) NOT NULL, 
    CHANGE `id_message` `message_id` INT(11) NOT NULL, 
    CHANGE `datelu` `moment` DATETIME NOT NULL;
-----------------------------------------------------------
ALTER TABLE `@prefix@api_tab_migration` CHANGE `dateMigration` `execution` DATETIME NOT NULL;
-----------------------------------------------------------
RENAME TABLE `@prefix@api_tab_parametres` TO `@prefix@api_tab_parameter`;
ALTER TABLE `@prefix@api_tab_parameter` 
    CHANGE `param_name` `name` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `param_type` `type` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `param_value` `value` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
;
UPDATE `@prefix@api_tab_parameter` SET `name` = 'instance_name' WHERE `id` = 1;
-----------------------------------------------------------
ALTER TABLE `@prefix@api_tab_service_mail` CHANGE `libelle` `label` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
-----------------------------------------------------------
ALTER TABLE `@prefix@api_tab_service_mail_dest` 
    CHANGE `id_type_mail` `mail_id` INT(11) NOT NULL,
    CHANGE `code_user` `user_code` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `nivo` `level` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
;
-----------------------------------------------------------
ALTER TABLE `@prefix@api_tab_session` 
    CHANGE `dateCreation` `creation` DATETIME NOT NULL, 
    CHANGE `periodeValideMinute` `ttl` INT(11) NOT NULL
;
-----------------------------------------------------------
RENAME TABLE `@prefix@api_tab_statistiques_site` TO `@prefix@api_tab_statistic`;
ALTER TABLE `@prefix@api_tab_statistic` ADD `type` VARCHAR(50) NOT NULL DEFAULT 'route' AFTER `id_user`;
ALTER TABLE `@prefix@api_tab_statistic` 
    CHANGE `id_user` `user_id` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `page` `context` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
;
-----------------------------------------------------------
ALTER TABLE `@prefix@api_tab_transaction` 
    CHANGE `statut` `status` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
    CHANGE `debut` `start` DATETIME NOT NULL, 
    CHANGE `fin` `end` DATETIME NOT NULL
;
-----------------------------------------------------------
SET FOREIGN_KEY_CHECKS=1;