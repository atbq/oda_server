# Change Log

## [1.0.29] - 2019-12-10

* fix(OdaMigration): close #15, secure and debug

## [1.0.28] - 2019-10-08

* fix(OdaLib): uncryptODA pb with quote

## [1.0.27] - 2019-10-08

* refacto(OdaLibInterface): ODADuree => duration, close #11

## [1.0.26] - 2019-09-27

* feat(OdaLibInterface):add playload input

## [1.0.25] - 2019-09-26

* feat(scripts):add possibility to disable scripts close #30

## [1.0.24] - 2019-09-25

* feat(SessionInterface):close #26, #27, add api session/invalid session/refresh

## [1.0.23] - 2019-09-25

* secu(SessionInterface):close #26, add check if the code&mail ok for send mail reset

## [1.0.22] - 2019-09-12

* fix(OdaLibInterface):fix in checkKey if referer not present yet

## [1.0.21] - 2019-09-11

* fix(uncryptODA):special char ...

## [1.0.20] - 2019-09-11

* feat(interface):add check origin

## [1.0.19] - 2019-09-09

* fix(composer):fix version for composer

## [1.0.18] - 2019-09-08

* fix(template):mail/resetPwd old tag script remove

## [1.0.17] - 2019-09-08

* feat(OdaLibInterface):store the signature off session

## [1.0.16] - 2019-09-08

* feat(OdaSession):add /session/resetPwd

## [1.0.15] - 2019-09-06

* add isTrue

## [1.0.14] - 2019-08-29

* add cryptODA

## [1.0.13] - 2019-08-26

* add uncrypted input with cryptODA = true

## [1.0.12] - 2019-03-01

* add modeSortie 'raw' for not print auto output

## [1.0.11] - 2019-02-28

* add multiple mail sendMailSES

## [1.0.10] - 2019-02-22

* add option param 'desc' for api POST /api/user/

## [1.0.9] - 2019-02-12

* Evol AWS SES for cross-account

## [1.0.8] - 2019-02-04

* add id of getall message

## [1.0.7] - 2019-01-30

* Refacto to push i8n tag in auth api

## [1.0.6] - 2019-01-21

* Fix issue #24 with buildsession

## [1.0.5] - 2019-01-03

* Fix issue with dep phpmyadmin/sql-parser, in 4.3.0 comma issue, fix to 4.2.x.