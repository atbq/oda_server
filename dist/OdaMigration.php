<?php
namespace Oda;
use
    stdClass,
    Exception,
    Oda\OdaLibBdn,
    JBZoo\Utils\Str,
    JBZoo\Utils\Filter,
    Oda\ExampleSortedIterator,
    Oda\SimpleObject\OdaConfig,
    Oda\SimpleObject\OdaPrepareReqSql,
    Oda\SimpleObject\OdaPrepareInterface
;
/**
 * OdaMigration Librairy - main class
 *
 * Tool
 *
 * @author  Fabrice Rosito <rosito.fabrice@gmail.com>
 * @version 0.171109.01
 */
class ExampleSortedIterator extends \SplHeap
{
    public function __construct(\Iterator $iterator)
    {
        foreach ($iterator as $item) {
            $this->insert($item);
        }
    }
    public function compare($b,$a)
    {
        return strcmp($a->getRealpath(), $b->getRealpath());
    }
}

class OdaMigration {
    /**
     * Content of config.php object $OdaConfig
     *
     * @var OdaConfig
     */
    protected static $config;
    /**
     * All details of the interface
     *
     * @var \ArrayObject
     */
    protected $params;
    /**
     * The bd engine
     *
     * @var OdaLibBd
     */
    protected $BD_ENGINE;
    /**
     * class constructor
     *
     * @param stdClass $p_params
     * @return OdaDate $this
     */
    public function __construct($p_params = NULL){
        self::$config = SimpleObject\OdaConfig::getInstance();
        self::$config->isOK();
        $params_bd = new stdClass();
        $params_bd->bd_conf = self::$config->BD_ENGINE;
        $this->BD_ENGINE = new OdaLibBd($params_bd);
        $this->params = $p_params;
    }
    /**
     * Destructor
     *
     * @access public
     * @return null
     */
    public function __destruct(){

    }

    /*
     * @return OdaMigration
     */
    public function migrate(){
        try {
            $this->isOk();

            if(isset($this->params['auto'])){
                print "Auto mode selected." . PHP_EOL;

                $params = new OdaPrepareReqSql();
                $params->sql = "SELECT `value`
                    FROM `".self::$config->BD_ENGINE->prefixTable."api_tab_parameter`
                    WHERE 1=1
                    AND `name` = 'install_date'
                ";
                $params->typeSQL = OdaLibBd::SQL_GET_ONE;
                $retour = $this->BD_ENGINE->reqODASQL($params);

                if($retour->data){

                    $installDate = $retour->data->value;
                    $compressInstallDate = Filter::int(Str::sub($installDate, 2, 2) . Str::sub($installDate, 5, 2) . Str::sub($installDate, 8, 2));

                    print "Install date is: " . $installDate . PHP_EOL;

                    $dit = new \RecursiveDirectoryIterator('.' . DIRECTORY_SEPARATOR, \RecursiveDirectoryIterator::SKIP_DOTS);
                    $rit = new \RecursiveIteratorIterator($dit, \RecursiveIteratorIterator::SELF_FIRST);
                    $sit = new ExampleSortedIterator($rit);

                    foreach($sit as $elt){
                        if ($elt->isDir()) {
                            $re = '~\b(-install|-seeds|-reworkModel|-matrixRangApi)\b~';
                            $ban = preg_match($re, $elt->getPathname(), $matches, PREG_OFFSET_CAPTURE, 0);
                            $contraintDate = preg_match('/[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])/', $elt->getPathname());
                            if($contraintDate){
                                $patchDate = Filter::int(Str::sub($elt->getPathname(), 6, 6));
                                $dateOk = ($patchDate > $compressInstallDate);
                                if($dateOk && empty($matches)) {
                                    $sql = $elt->getPathname() . DIRECTORY_SEPARATOR  . 'do.sql';
                                    if(file_exists($sql)){
                                        $this->exe($sql);
                                    }else{
                                        $php = $elt->getPathname() . DIRECTORY_SEPARATOR  . 'do.php';
                                        if(file_exists($php)){
                                            $this->exe($php, "php");
                                        }else{
                                            print "No script to execute for: {$elt->getPathname()}" . PHP_EOL;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    print "No install_date retrieve." . PHP_EOL;
                }
            }else{
                print "Target mode selected." . PHP_EOL;

                if($this->params['partial'] !== "all"){
                    $path = '.'.DIRECTORY_SEPARATOR.$this->params['target'].DIRECTORY_SEPARATOR.$this->params['partial'];
                    $sql = $path.DIRECTORY_SEPARATOR.$this->params['option'].'.sql';
                    if(file_exists($sql)){
                        $this->exe($sql);
                    }else{
                        $php = $path.DIRECTORY_SEPARATOR.$this->params['option'].'.php';
                        if(file_exists($php)){
                            $this->exe($php, "php");
                        }else{
                            print "No script to execute for: {$path}" . PHP_EOL;
                        }
                    }
                }else{
                    $dit = new \RecursiveDirectoryIterator('.' . DIRECTORY_SEPARATOR . $this->params['target'], \RecursiveDirectoryIterator::SKIP_DOTS);
                    $rit = new \RecursiveIteratorIterator($dit, \RecursiveIteratorIterator::SELF_FIRST);
                    $sit = new ExampleSortedIterator($rit);

                    foreach($sit as $elt){
                        if ($elt->isDir() && ($elt->getPath() != $this->params['partial'])) {
                            $sql = $elt->getPathname().DIRECTORY_SEPARATOR.$this->params['option'].'.sql';
                            if(file_exists($sql)){
                                $this->exe($sql);
                            }else{
                                $php = $elt->getPathname().DIRECTORY_SEPARATOR.$this->params['option'].'.php';
                                if(file_exists($php)){
                                    $this->exe($php, "php");
                                }else{
                                    print "No script to execute for: {$elt->getPathname()}" . PHP_EOL;
                                }
                            }
                        }
                    }
                }
            }

            echo 'Finish' . PHP_EOL;

            return $this;
        } catch (Exception $ex) {
            die($ex.'');
        }
    }
    /**
     * isOK
     *
     * @access public
     * @return boolean
     */
    public function isOK(){
        try {
            if (is_null($this->params)||empty($this->params)) {
                print "Options are missing." . PHP_EOL;
                print "|__ 'auto' => ex: --auto" . PHP_EOL;
                print "|__ 'target' => ex: --target=000-install | all." . PHP_EOL;
                print "   |__ 'partial', optional => ex: --partial=000-useful" . PHP_EOL;
                print "   |__ 'option', optional => ex: --option=do | unDo" . PHP_EOL;
                print "   |__ 'checkDb', optional => ex: --checkDb" . PHP_EOL;
                die(1);
            }

            if ((!isset($this->params['auto'])&&(!isset($this->params['target'])))) {
                print "Option 'Target' and 'auto' is missing." . PHP_EOL;
                die(1);
            }

            if ((isset($this->params['target'])&&(empty($this->params['target'])))) {
                print "Option 'Target' is not defined." . PHP_EOL;
                die(1);
            }

            if (!isset($this->params['option']) ) {
                $this->params['option'] = "do";
            }

            if (!isset($this->params['partial']) ) {
                $this->params['partial'] = "all";
            }

            if (!isset($this->params['checkDb']) ) {
                $this->params['checkDb'] = false;
            }

            if (isset($this->params['auto']) ) {
                $this->params['checkDb'] = true;
            }
            return true;
        } catch (Exception $ex) {
            die($ex.'');
        }
    }
    /**
     * exe
     *
     * @access public
     * @return boolean
     */
    public function exe($file, $type = "sql"){
        try {
            echo "Script selected : ". $file . PHP_EOL;

            if($this->params['checkDb']){
                $params = new OdaPrepareReqSql();
                $params->sql = "
                    SELECT COUNT(*) as 'nb'
                    FROM `".self::$config->BD_ENGINE->prefixTable."api_tab_migration`
                    WHERE 1=1
                    AND `name` = '".str_replace('\\', '/', $file)."'
                ";
                $params->typeSQL = OdaLibBd::SQL_GET_ONE;
                if(isset($this->params['debug'])){
                    $params->debug = true;
                }
                $retour = $this->BD_ENGINE->reqODASQL($params);

                if($retour->strErreur !== ''){
                    throw new Exception("Migration error: " . $retour->strErreur);
                }

                $exist = $retour->data->nb;

                if($exist && ($this->params['option'] == "do")){
                    echo "Status for the migration: $file: already done" . PHP_EOL;
                    return true;
                }else if(!$exist && ($this->params['option'] == "do")){
                    echo "Status for the migration: $file: clear to done" . PHP_EOL;
                }

                if(!$exist && ($this->params['option'] == "unDo")){
                    echo "Status for the migration: $file: nothing to unDo" . PHP_EOL;
                    return true;
                }else if($exist && ($this->params['option'] == "unDo")){
                    echo "Status for the migration: $file: check ok to unDo" . PHP_EOL;
                }
            }

            if($type == "sql"){
                $contentScript = file_get_contents($file, FILE_USE_INCLUDE_PATH);

                $contentScript = str_replace("@prefix@", self::$config->BD_ENGINE->prefixTable, $contentScript);

                $params = new OdaPrepareReqSql();
                $params->sql = $contentScript;
                $params->typeSQL = OdaLibBd::SQL_SCRIPT;

                $parser = new \PhpMyAdmin\SqlParser\Parser($contentScript);
                
                if($parser->errors){
                    echo "DEBUG: get sql script parser" . PHP_EOL;
                    var_dump($parser->errors);

                    $retour = new stdClass();
                    $retour->strStatut = 4;
                    $retour->strErreur = $parser->errors[0]->getMessage();
                }else{
                    if(isset($this->params['debug'])){
                        $params->debug = true;
                    }
                    $retour = $this->BD_ENGINE->reqODASQL($params);

                    if($retour->strErreur !== ''){
                        throw new Exception("Migration script error: " . $retour->strErreur);
                    }
                }
            }elseif($type == "php"){
                exec('php ' . $file, $output, $return);
                
                if(isset($this->params['debug'])){
                    echo "DEBUG: get php script return" . PHP_EOL;
                    var_dump($output);
                    var_dump($return);
                }

                if ($return != 0) {
                    var_dump($output);
                    var_dump($return);
                    // an error occurred
                    throw new Exception("Migration exec php error");
                }

                $retour = new stdClass();
                $retour->strStatut = 5;
                $retour->strErreur = "";
            }else{
                $retour = new stdClass();
                $retour->strStatut = 4;
                $retour->strErreur = "Type: ". $type . " unknown.";
            }

            echo "Status for the migration: " . $retour->strStatut . (($retour->strStatut !== 5) ? (" (error : " . $retour->strErreur . ")") : "")    . PHP_EOL;

            if($retour->strStatut == 5){
                if($this->params['option'] == "do"){
                    $params = new OdaPrepareReqSql();
                    $params->sql = "
                        INSERT INTO `".self::$config->BD_ENGINE->prefixTable."api_tab_migration`
                        (`name`, `execution`)
                        VALUES
                        ('".str_replace('\\', '/', $file)."', NOW())
                    ";
                    $params->typeSQL = OdaLibBd::SQL_SCRIPT;
                    if(isset($this->params['debug'])){
                        $params->debug = true;
                    }
                    $retour = $this->BD_ENGINE->reqODASQL($params);

                    echo "Status for the trace record : " . $retour->strStatut . (($retour->strStatut !== 5) ? (" (error : " . $retour->strErreur . ")") : "")    . PHP_EOL;
                }elseif($this->params['option'] == "unDo"){
                    $params = new OdaPrepareReqSql();
                    $file = str_replace('unDo','do',$file);
                    $params->sql = "
                        DELETE FROM `".self::$config->BD_ENGINE->prefixTable."api_tab_migration`
                        WHERE 1=1
                        AND `name` = '".str_replace('\\', '/', $file)."'
                    ";
                    $params->typeSQL = OdaLibBd::SQL_SCRIPT;
                    if(isset($this->params['debug'])){
                        $params->debug = true;
                    }
                    $retour = $this->BD_ENGINE->reqODASQL($params);

                    echo "Status for the trace record : " . $retour->strStatut . (($retour->strStatut !== 5) ? (" (error : " . $retour->strErreur . ")") : "")    . PHP_EOL;
                }
            }
            return $this;
        } catch (Exception $ex) {
            die($ex.'');
        }
    }
}