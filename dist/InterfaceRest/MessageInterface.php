<?php
namespace Oda\InterfaceRest;

use Exception,
    stdClass, 
    Oda\OdaLibBd,
    Oda\OdaRestInterface,
    Oda\SimpleObject\OdaPrepareInterface, 
    Oda\SimpleObject\OdaPrepareReqSql
;

/**
 * @author  Fabrice Rosito <rosito.fabrice@gmail.com>
 * @version 0.17050200
 */
class MessageInterface extends OdaRestInterface {
    /**
     */
    function create(){
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "INSERT INTO `api_tab_message`
                (`active`, `message`, `rank_id`, `level`, `expiration`, `user_id`, `creation`)
                VALUES 
                ( 1, :message, :rankId, :level, :expirationDate, :userId, NOW() )
            ;";
            $params->bindsValue = [
                "userId" => $this->inputs["userId"],
                "message" => $this->inputs["message"],
                "level" => $this->inputs["level"],
                "expirationDate" => $this->inputs["expirationDate"],
                "rankId" => $this->inputs["rankId"]
            ];
            $params->typeSQL = OdaLibBd::SQL_INSERT_ONE;
            $retour = $this->BD_ENGINE->reqODASQL($params);

            $params = new stdClass();
            $params->retourSql = $retour;
            $this->addDataReqSQL($params);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     */
    function getAll(){
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT a.`id`, a.`message`, a.`level`, a.`expiration`, a.`creation`, a.`creation`, a.`active`,
                b.`label` as 'rank_label',
                c.`code` as 'user_code'
                FROM `api_tab_message` a, `api_tab_rank` b, `api_tab_user` c
                WHERE 1=1
                AND a.`rank_id` = b.`id`
                AND a.`user_id` = c.`id`
                ORDER BY a.`id` DESC
                LIMIT 0, 10
            ;";
            $params->typeSQL = OdaLibBd::SQL_GET_ALL;
            $retour = $this->BD_ENGINE->reqODASQL($params);
            
            $params = new stdClass();
            $params->retourSql = $retour;
            $this->addDataObject($retour->data->data);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     */
    function getForCurrentUser(){
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT a.`id`, a.`level`, a.`message`
                FROM `api_tab_message` a, `api_tab_rank` e
                WHERE 1=1
                AND a.`rank_id` = e.`id`
                AND NOT EXISTS (
                    SELECT 1
                    FROM  `api_tab_message_read` b
                    WHERE 1=1
                    AND a.`id` = b.`message_id`
                    AND b.`user_id` = :userId
                )
                AND e.`value` >= (
                    SELECT d.`value`
                    FROM `api_tab_user` c, `api_tab_rank` d
                    WHERE 1=1
                    AND c.`rank_id` = d.`id`
                    AND c.`id` = :userId
                )
                AND IF(a.`expiration` != '0000-00-00', a.`expiration` > NOW(), (a.`creation` + INTERVAL 7 DAY) > NOW())
                ORDER BY a.`id` desc
                LIMIT 0, 10
            ;";
            $params->bindsValue = [
                "userId" => $this->user->id
            ];
            $params->typeSQL = OdaLibBd::SQL_GET_ALL;
            $retour = $this->BD_ENGINE->reqODASQL($params);
            
            $params = new stdClass();
            $params->retourSql = $retour;
            $this->addDataObject($retour->data->data);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     */
    function setReadForCurrentUser($messageId){
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "INSERT INTO `api_tab_message_read` (`user_id`, `message_id`, `moment`)
                SELECT :user_id, a.`id` , NOW()
                FROM `api_tab_message` a, `api_tab_rank` e
                WHERE 1=1
                AND a.`rank_id` = e.`id`
                AND NOT EXISTS (
                    SELECT 1
                    FROM  `api_tab_message_read` b
                    WHERE 1=1
                    AND a.`id` = b.`message_id`
                    AND b.`user_id` = :user_id
                )
                AND e.`value` >= (
                    SELECT d.`value`
                    FROM `api_tab_user` c, `api_tab_rank` d
                    WHERE 1=1
                    AND c.`rank_id` = d.`id`
                    AND c.`id` = :user_id
                )
                AND IF(a.`expiration` != '0000-00-00', a.`expiration` > NOW(), (a.`creation` + INTERVAL 7 DAY) > NOW())
                ORDER BY a.`id` desc
                LIMIT 0, 10
            ;";
            $params->bindsValue = [
                "user_id" => $this->user->id
            ];
            $params->typeSQL = OdaLibBd::SQL_INSERT_ONE;
            $retour = $this->BD_ENGINE->reqODASQL($params);

            $params = new stdClass();
            $params->retourSql = $retour;
            $this->addDataReqSQL($params);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
}