<?php
namespace Oda\InterfaceRest;

use 
    stdClass,    
    Exception,
    Oda\OdaLibBd,
    Oda\OdaRestInterface,
    Oda\SimpleObject\OdaPrepareReqSql
;

/**
 * @author  Fabrice Rosito <rosito.fabrice@atbq.fr>
 * @version 180220
 */
class UserInterface extends OdaRestInterface {
    
    /**
     * @desc search
     */
    function search() {
        try {
            $params = new stdClass();
            $params->code = $this->inputs["code"];
            $params->mail = $this->inputs["mail"];
            $response = self::_search($this, $params);
            $this->addDataObject($response);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }

    /**
     */
    function getAll(){
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT a.`code`, a.`code` as 'code_user', a.`mail`, a.`name_first`, a.`name_first` as 'prenom', a.`name_last`, a.`name_last` as 'nom', a.`description`, a.`active`, a.`active` as 'actif',
                b.`label` as 'rank_label', b.`label` as 'labelle', b.`value` as 'rank_value' , b.`value` as 'rankIndice' 
                FROM `api_tab_user` a, `api_tab_rank` b
                WHERE 1=1
                AND a.`rank_id` = b.`id`
                ORDER BY a.`active`, a.`code`
            ;";
            $params->typeSQL = OdaLibBd::SQL_GET_ALL;
            $retour = $this->BD_ENGINE->reqODASQL($params);
            $this->addDataObject($retour->data->data);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     */
    function getAllMail(){
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT a.`code`, a.`code` as 'code_user', a.`mail`
                FROM `api_tab_user` a
                WHERE 1=1
                AND a.`active` = 1
                ORDER BY a.`code`
            ;";
            $params->typeSQL = OdaLibBd::SQL_GET_ALL;
            $retour = $this->BD_ENGINE->reqODASQL($params);
            $this->addDataObject($retour->data->data);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     */
    function getByMail(){
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT a.`code`, a.`code` as 'code_user'
                FROM `api_tab_user` a
                WHERE 1=1
                AND a.`mail` = :email
            ;";
            $params->bindsValue = [
                "email" => $this->inputs["email"]
            ];
            $params->typeSQL = OdaLibBd::SQL_GET_ALL;
            $retour = $this->BD_ENGINE->reqODASQL($params);
            $this->addDataObject($retour->data->data);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     */
    function create(){
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT count(*) as result
                from `api_tab_user` a
                where 1=1
                AND a.`code` like '".$this->inputs["userCode"]."%'
            ;";
            $params->typeSQL = OdaLibBd::SQL_GET_ONE;
            $retour = $this->BD_ENGINE->reqODASQL($params);
            $nbSamePseudo = intval($retour->data->result);

            //--------------------------------------------------------------------------
            if($nbSamePseudo == 0){
                $userCode = $this->inputs["userCode"];
            }else{
                $userCode = $this->inputs["userCode"].$nbSamePseudo;
            }

            $desc = "";
            if($this->inputs["desc"] !== null){
                $desc = $this->inputs["desc"];
            }

            $params = new OdaPrepareReqSql();
            $params->sql = "INSERT INTO `api_tab_user` 
                (`password`,`code`,`name_first`,`name_last`, `description`, `mail`,`active`,`creation`)
                VALUES  
                ( :password, :userCode, :firstName, :lastName, :desc, :mail, 1, now())
            ;";
            $params->bindsValue = [
                "firstName" => $this->inputs["firstName"],
                "lastName" => $this->inputs["lastName"],
                "mail" => $this->inputs["mail"],
                "password" => password_hash($this->inputs["password"], PASSWORD_DEFAULT),
                "userCode" => $userCode,
                "desc" => $desc
            ];
            $params->typeSQL = OdaLibBd::SQL_INSERT_ONE;
            //$params->debug = true;
            $retour = $this->BD_ENGINE->reqODASQL($params);

            $params = new stdClass();
            $params->id = $retour->data;
            $params->userCode = $userCode;
            $params->mail = $this->inputs["mail"];
            $this->addDataObject($params);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     */
    function resetPwd() {
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "UPDATE `api_tab_user`
                SET
                    `password`= :pwd
                WHERE 1=1
                  AND `code` = :userCode
                  AND `mail` = :mail
            ;";
            $params->bindsValue = [
                "userCode" => $this->inputs["userCode"],
                "pwd" =>  password_hash($this->inputs["pwd"], PASSWORD_DEFAULT),
                "mail" => $this->inputs["email"]
            ];
            $params->typeSQL = OdaLibBd::SQL_SCRIPT;
            $retour = $this->BD_ENGINE->reqODASQL($params);

            $params = new stdClass();
            $params->value = $retour->data;
            $this->addDataStr($params);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     */
    function getCurrent(){
        try {
            $this->getByCode($this->user->codeUser);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
     }
    /**
     */
    function getByCode($userCode){
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT a.`id`, a.`id` as 'id_user', a.`code`, a.`code` as 'code_user', a.`name_last`, a.`name_last` as 'nom', a.`name_first`, a.`name_first` as 'prenom', a.`rank_id`, a.`rank_id` as 'id_rang', 
                a.`mail`, a.`language`, a.`language` as 'langue', a.`description`, a.`active`, a.`active` as 'actif',
                b.`value` as 'rank_value', b.`value` as 'profile', b.`label` as 'rank_label', b.`label` as 'labelle'
                FROM `api_tab_user` a, `api_tab_rank` b
                WHERE 1=1 
                AND a.`rank_id` = b.`id`
                AND a.`code` = :code_user
            ;";
            $params->bindsValue = [
                "code_user" => $userCode
            ];
            $params->typeSQL = OdaLibBd::SQL_GET_ONE;
            $retour = $this->BD_ENGINE->reqODASQL($params);
            $this->addDataObject($retour->data);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
     }
    /**
      */
    function updateUser($userCode) {
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "UPDATE `api_tab_user`
                SET
                    `rank_id`= :rankId,
                    `mail`= :mail,
                    `active`= :active,
                    `description`= :desc
                WHERE 1=1
                  AND `code` = :userCode
            ;";
            $params->bindsValue = [
                "userCode" => $userCode,
                "mail" => $this->inputs["mail"],
                "active" => $this->inputs["active"],
                "rankId" => $this->inputs["rankId"],
                "desc" => $this->inputs["desc"]
            ];
            $params->typeSQL = OdaLibBd::SQL_SCRIPT;
            $retour = $this->BD_ENGINE->reqODASQL($params);

            $params = new stdClass();
            $params->value = $retour->data;
            $this->addDataStr($params);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     */
    function getActivity(){
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT IF(b.`code`='','N.A',b.`code`) as 'code_user', IF(b.`code`='','N.A',b.`code`) as 'code', count(*) 'nb', count(*) 'nombre'
                FROM `api_tab_statistic` a, `api_tab_user` b, `api_tab_rank` c
                WHERE 1=1
                AND a.`user_id` = b.`id`
                AND b.`rank_id` = c.`id`
                AND c.`value` > 1
                GROUP BY a.`user_id`
                ORDER BY `nombre` desc
            ;";
            $params->typeSQL = OdaLibBd::SQL_GET_ALL;
            $retour = $this->BD_ENGINE->reqODASQL($params);
            $this->addDataObject($retour->data->data);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
      */
    function updateField() {
        try {
            $params = new stdClass();
            $params->nameObj = "api_tab_user";
            $params->keyObj = ["code" => $this->user->codeUser];
            $retour = $this->BD_ENGINE->getSingleObject($params);

            if(!password_verify($this->inputs["password"], $retour->password)){
                $this->dieInError('Wrong password.');
            }else{
                $params = new stdClass();
                $params->nameObj = "api_tab_user";
                $params->keyObj = ["code" => $this->user->codeUser];
                $params->setObj = [$this->inputs["field"] => $this->inputs["value"]];
                $retour = $this->BD_ENGINE->setSingleObj($params);
                $this->addDataStr($retour);
            }
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }

    /**
     * INTERNALS
     */

    /**
     * internal
     * return data if user exist
     */
    static function _search($instance, $p){
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT a.`code`, a.`mail`, a.`name_last`, a.`name_first`
                FROM `api_tab_user` a
                WHERE 1=1
                AND a.`mail` = :mail
                AND a.`code` = :code
            ;";
            $params->bindsValue = [
                "mail" => $p->mail,
                "code" => $p->code
            ];
            $params->typeSQL = OdaLibBd::SQL_GET_ONE;
            $retour = $instance->BD_ENGINE->reqODASQL($params);
            return $retour->data;
        } catch (Exception $ex) {
            $instance->dieInError($ex.'');
        }
    }
}