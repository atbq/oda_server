<?php
namespace Oda\InterfaceRest;

use 
    stdClass,
    Exception,
    Oda\OdaLib,
    Oda\OdaLibBd,
    Oda\OdaTemplate,
    Oda\OdaRestInterface,
    Oda\SimpleObject\OdaConfig,
    Oda\SimpleObject\OdaPrepareReqSql,
    Oda\SimpleObject\OdaPrepareInterface
;

/**
 * SessionInterface
 *
 * @author  Fabrice Rosito <rosito.fabrice@gmail.com>
 * @version 0.1702280
 */
class SessionInterface extends OdaRestInterface {
    
    /**
     * @desc resetPwd
     */
    function resetPwd() {
        try {
            $params = new stdClass();
            $params->code = $this->inputs["code"];
            $params->mail = $this->inputs["mail"];
            $response = self::_resetPwd($this, $params);
            $this->addDataObject($response);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }

    /**
     * @desc invalid
     */
    function invalid() {
        try {
            $params = new stdClass();
            $params->code = $this->inputs["code"];
            $response = self::_invalid($this, $params);
            $this->addDataObject($response);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }

    /**
     * @desc refresh
     */
    function refresh() {
        try {
            $params = new stdClass();
            $response = self::_refresh($this, $params);
            $this->addDataObject($response);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    
    /**
     */
    function create(){
        try {
            //--------------------------------------------------------------------------
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT a.`rank_id`, a.`code`, a.`password`, a.`mail`, a.`active`
                FROM `api_tab_user` a
                WHERE 1=1
                AND a.`code` = :code_user
            ;";
            $params->bindsValue = [
                "code_user" => $this->inputs["userCode"]
            ];
            $params->typeSQL = OdaLibBd::SQL_GET_ONE;
            $retour = $this->BD_ENGINE->reqODASQL($params);

            if(!$retour->data){
                $this->dieInError('oda-main.auth-fail-user-unknown', $this::STATE_ERROR_AUTH);
            }elseif($retour->data->active == "0"){
                $this->dieInError('oda-main.auth-fail-user-disable', $this::STATE_ERROR_AUTH);
            }else{
                if(OdaLib::startsWith($this->inputs["password"],"authByGoogle-")){
                    $mail = str_replace("authByGoogle-", "", $this->inputs["password"]);
                    if($mail !== $retour->data->mail){
                        $this->dieInError('oda-main.auth-fail-mail-wrong', $this::STATE_ERROR_AUTH);
                    }
                }
            }

            $key = $this->buildSession(array(
                'code_user' => $this->inputs["userCode"], 
                'password' => $this->inputs["password"], 
                'sessionTimeOutMinute' => $this->inputs["sessionTimeOutMinute"], 
                'dbPassword' => $retour->data->password)
            );

            $data = new stdClass();
            $data->id_rang = $retour->data->rank_id;
            $data->code_user = $retour->data->code;
            $data->keyAuthODA = $key;
            $retour->data = $data;

            //--------------------------------------------------------------------------
            $params = new stdClass();
            $params->retourSql = $retour;
            $this->addDataReqSQL($params);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     */
    function getBykey($key) {
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT a.`id`, a.`datas`, a.`creation`, a.`creation` as 'dateCreation', a.`ttl`, a.`ttl` as 'periodeValideMinute'
                FROM `api_tab_session` a
                WHERE 1=1
                AND a.`key` = :key
            ;";
            $params->bindsValue = [
                "key" => $key
            ];
            $params->typeSQL = OdaLibBd::SQL_GET_ONE;
            $retour = $this->BD_ENGINE->reqODASQL($params);

            $params = new stdClass();
            $params->retourSql = $retour;
            $this->addDataObject($retour->data);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     */
    function check() {
        try {
            $retour = $this->checkSession($this->inputs);
            $params = new stdClass();
            $params->value = $retour;
            $this->addDataStr($params);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     */
    function delete($key) {
        try {
            $retour = $this->deleteSession($key);
            $params = new stdClass();
            $params->value = $retour;
            $this->addDataStr($params);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }

    /**
     * INTERNALS
     */

    /**
     * internal
     * send mail with url token
     */
    function _resetPwd($instance, $p) {
        try {
            $response = new stdClass();
            $response->mailStatus = false;
            $search = UserInterface::_search($instance, $p);

            if($search){
                $config = OdaConfig::getInstance();

                $date = time() + 60 * 60000;

                $token = array(
                    'userCode' => $p->code,
                    'valideDate' => $date
                );

                $token = OdaLib::cryptODA($token);
                
                $host = (isset($config->urlHost))?$config->urlHost:$instance->slim->request()->getHost();

                $urlReset = $host . "#resetPwd?token=" . $token;

                $title = new OdaTemplate(dirname(__DIR__) . "/../resources/template/mail/resetPwd/subject.fr.tpl.html");
                $title = $title->output();
                $body = new OdaTemplate(dirname(__DIR__) . "/../resources/template/mail/resetPwd/body.fr.tpl.html");
                $body->set("code", $p->code);
                $body->set("urlReset", $urlReset);
                $body = $body->output();

                $params = new stdClass();
                $params->from = $instance->getParameter('contact_mail_administrateur');
                $params->to = $p->mail;
                $params->subject = $title;
                $params->bodyHtml = $body;
                $result = OdaLib::mail($params);

                $response->mailStatus = $result;
            }
            
            return $response->mailStatus;
        } catch (Exception $ex) {
            $instance->dieInError($ex.'');
        }
    }

    /**
     * internal
     * admin can invalid session of user
     */
    function _invalid($instance, $p) {
        try {
            $response = new stdClass();
            $response->nb = 0;

            //search
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT a.`id`, a.`key`, a.`datas`
                FROM `api_tab_session` a
                WHERE 1=1
                AND a.`datas` like concat('%', :code, '%')
            ;";
            $params->bindsValue = [
                "code" => $p->code
            ];
            $params->typeSQL = OdaLibBd::SQL_GET_ALL;
            $result = $instance->BD_ENGINE->reqODASQL($params);

            //remove
            if($result->data){
                foreach ($result->data->data as $value){
                    $result = $instance->deleteSession($value->key);
                    if($result){
                        $response->nb++;
                    }
                }
            }
            
            return $response;
        } catch (Exception $ex) {
            $instance->dieInError($ex.'');
        }
    }

    /**
     * internal
     * return new token
     */
    function _refresh($instance, $p) {
        try {
            $response = new stdClass();

            //build token
            $params = new stdClass();
            $params->code = $instance->user->codeUser;
            $token = self::_createToken($instance, $params);
            $response->key = $token->key;

            $params = new OdaPrepareReqSql();
            $params->sql = "UPDATE `api_tab_session`
                SET `key` = :key,
                    `datas` = :datas
                WHERE `key` = :oldKey
            ;";
            $params->typeSQL = OdaLibBd::SQL_SCRIPT;
            $params->bindsValue = [
                "key" => $token->key,
                "datas" => $token->datas,
                "oldKey" => $instance->inputs["keyAuthODA"]
            ];
            //$params->debug = true;
            $result = $instance->BD_ENGINE->reqODASQL($params);
            if(!$result->data){
                throw new Exception('Error while updating key');
            }
            
            return $response;
        } catch (Exception $ex) {
            $instance->dieInError($ex.'');
        }
    }
}