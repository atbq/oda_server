<?php
namespace Oda\InterfaceRest;

use Exception;
use Oda\OdaLibBd;
use Oda\OdaRestInterface;
use Oda\SimpleObject\OdaPrepareReqSql;
use \stdClass;

/**
 * @author  Fabrice Rosito <rosito.fabrice@gmail.com>
 * @version 0.170421
 */
class NavigationInterface extends OdaRestInterface {
    /**
     */
    function getAllPage() {
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT a.`id`, a.`label`, a.`description`, a.`category_id`, a.`route`, a.`route` as 'link',
                b.`description` as 'category_desc'
                FROM `api_tab_menu` a, `api_tab_menu_category` b
                WHERE 1=1
                AND a.`category_id` = b.`id`
                ORDER BY a.`id` ASC
            ;";
            $params->typeSQL = OdaLibBd::SQL_GET_ALL;
            $retour = $this->BD_ENGINE->reqODASQL($params);

            $params = new stdClass();
            $params->retourSql = $retour;
            $this->addDataObject($retour->data);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     */
    function getAllRank() {
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT a.`id`, a.`label`, a.`value`, a.`value` as 'index'
                FROM `api_tab_rank` a
                WHERE 1=1
                ORDER BY a.`value` ASC
            ;";
            $params->typeSQL = OdaLibBd::SQL_GET_ALL;
            $retour = $this->BD_ENGINE->reqODASQL($params);

            $params = new stdClass();
            $params->retourSql = $retour;
            $this->addDataObject($retour->data);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     */
    function getRight() {
        try {
            $filterRank = "";
            if($this->user->indice != "1"){
                $params = new OdaPrepareReqSql();
                $params->sql = "SELECT a.`id`
                    FROM  `api_tab_rank` a
                    WHERE 1=1
                    AND a.`value` = :rang
                ;";
                $params->bindsValue = [
                    "rang" => $this->user->indice
                ];
                $params->typeSQL = OdaLibBd::SQL_GET_ONE;
                $retour = $this->BD_ENGINE->reqODASQL($params);
                $id_rang = $retour->data->id;
                $filterRank = "AND EXISTS (SELECT 1 FROM `api_tab_menu_rank` c WHERE c.`rank_id` = ".$id_rang." AND c.`menu_ids` LIKE CONCAT('%;',a.`id`,';%'))";
            }

            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT a.`id`, a.`label`, a.`label` as 'Description_courte', a.`description`, a.`description` as 'Description_menu', a.`route`, a.`route` as 'Lien', a.`category_id`, a.`category_id` as 'id_categorie',
                b.`description` as 'category_desc', b.`description` as 'Description_cate'
                FROM  `api_tab_menu` a, `api_tab_menu_category` b
                WHERE 1=1
                AND a.`category_id` = b.id
                ".$filterRank."
                ORDER BY a.`category_id` asc, a.`label` asc
            ;";
            $params->typeSQL = OdaLibBd::SQL_GET_ALL;
            $retour = $this->BD_ENGINE->reqODASQL($params);

            $params = new stdClass();
            $params->retourSql = $retour;
            $this->addDataObject($retour->data->data);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     */
    function getRights() {
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT a.`id` , a.`rank_id`, a.`menu_ids`
                FROM `api_tab_menu_rank` a
                WHERE 1=1
            ;";
            $params->typeSQL = OdaLibBd::SQL_GET_ALL;
            $retour = $this->BD_ENGINE->reqODASQL($params);

            $params = new stdClass();
            $params->retourSql = $retour;
            $this->addDataObject($retour->data);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     */
    function updateRight($id) {
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "UPDATE `api_tab_menu_rank`
                SET
                    `menu_ids`= :value
                WHERE 1=1
                  AND `id` = :id
            ;";
            $params->bindsValue = [
                "value" => $this->inputs["value"],
                "id" => $id
            ];
            $params->typeSQL = OdaLibBd::SQL_SCRIPT;
            $retour = $this->BD_ENGINE->reqODASQL($params);

            $params = new stdClass();
            $params->value = $retour->data;
            $this->addDataStr($params);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
}