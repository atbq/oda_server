<?php
namespace Oda\InterfaceRest;

use 
    stdClass, 
    Exception,
    Oda\OdaLib,
    Oda\OdaLibBd,
    Oda\OdaRestInterface,
    Oda\SimpleObject\OdaPrepareInterface, 
    Oda\SimpleObject\OdaPrepareReqSql
;

/**
 * @author  Fabrice Rosito <rosito.fabrice@atbq.fr>
 * @version 180222
 */
class SystemInterface extends OdaRestInterface {
    /**
     */
    function createPageTrace(){
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "INSERT INTO `api_tab_statistic`
                (`date`, `user_id`, `context`, `action`)
                SELECT NOW(),`id`, :page, :nature
                FROM `api_tab_user`
                WHERE 1=1
                AND `code` = :user
            ;";
            $params->bindsValue = [
                "user" => $this->inputs["user"],
                "page" => $this->inputs["page"],
                "nature" => $this->inputs["action"]
            ];
            $params->typeSQL = OdaLibBd::SQL_INSERT_ONE;
            $retour = $this->BD_ENGINE->reqODASQL($params);

            $params = new stdClass();
            $params->retourSql = $retour;
            $this->addDataReqSQL($params);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }

    /**
      */
    function getReportPageActivity(){
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT a.`context`, a.`context` as 'page', count(*) 'nb', count(*) 'nombre'
                FROM `api_tab_statistic` a, `api_tab_user` b, `api_tab_rank` c
                WHERE 1=1
                AND a.`user_id` = b.`id`
                AND b.`rank_id` = c.`id`
                AND a.`type` = 'route'
                AND c.`value` > 1
                GROUP BY a.`context`
                ORDER BY `nb` desc
            ;";
            $params->typeSQL = OdaLibBd::SQL_GET_ALL;
            $retour = $this->BD_ENGINE->reqODASQL($params);
            $this->addDataObject($retour->data->data);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    
    /**
      */
    function getAllTheme(){
        try {
            $theme_defaut = $this->getParameter("theme_defaut");

            $params = new stdClass();
            $params->label = "theme";
            if(is_null($theme_defaut)){
                $params->value = "notAvailable";
            }else{
                $params->value = $theme_defaut;
            }
            $this->addDataStr($params);

            //--------------------------------------------------------------------------
            $params = new stdClass();
            $params->nameObj = "api_tab_user";
            $params->keyObj = ["code" => $this->inputs["code_user"]];
            $params->debug = false;
            $retour = $this->BD_ENGINE->getSingleObject($params);

            $params = new stdClass();
            $params->label = "themePerso";
            if(!isset($retour->theme)){
                $params->value = "notAvailable";
            }else{
                $params->value = $retour->theme;
            }
            $this->addDataStr($params);

            //--------------------------------------------------------------------------
            $path = '../../css/themes/';

            $liste_theme = array();

            $theme = new stdClass();
            $theme -> nom = 'default';
            $theme -> path = 'vendor/atbq/oda/resources/api/css/themes/';
            $liste_theme[] = $theme;

            $dir = opendir($path); 
            while($file = readdir($dir)) {
                if(is_dir($path.$file)){
                    if(($file != '.') && ($file != '..')){
                        $theme = new \stdClass();
                        $theme -> nom = $file;
                        $theme -> path = 'css/themes/';
                        $liste_theme[] = $theme;
                    }
                }
            }
            closedir($dir);

            $params = new \stdClass();
            $params->label = "listTheme";
            $params->value = $liste_theme;
            $this->addDataStr($params);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    
    /**
      */
    function cleanDb(){
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT 'api_tab_session' as 'table', COUNT(*) as 'nb'
            FROM `api_tab_session`
            UNION
            SELECT 'api_tab_transaction' as 'table', COUNT(*) as 'nb'
            FROM `api_tab_transaction`
            UNION
            SELECT 'api_tab_log' as 'table', COUNT(*) as 'nb'
            FROM `api_tab_log`
            ;";
            $params->typeSQL = OdaLibBd::SQL_GET_ALL;
            $v_resultats = $this->BD_ENGINE->reqODASQL($params);

            $nb_api_tab_session = intval($v_resultats->data->data[0]->nb);
            $nb_api_tab_transaction = intval($v_resultats->data->data[1]->nb);
            $nb_api_tab_log = intval($v_resultats->data->data[2]->nb);

            $params = new stdClass();
            $params->label = "resultat";
            $params->retourSql = $v_resultats;
            $this->addDataReqSQL($params);

            //--------------------------------------------------------------------------
            $params = new stdClass();
            $params->label = "exec";
            $params->value = $this->inputs["exec"];
            $this->addDataStr($params);

            //--------------------------------------------------------------------------
            $array_purges = array();
            if($this->inputs["exec"] == "true"){
                //--------------------------------------------------------------------------
                // Purge api_tab_session
                $obj_purge = new stdClass();
                $obj_purge->table = "api_tab_session";
                $obj_purge->nb = 0;
                $obj_purge->statut = "none";
                if($nb_api_tab_session > 1000){
                    $obj_purge->statut = "init";

                    $params = new OdaPrepareReqSql();
                    $params->sql = "DELETE
                        FROM `api_tab_session`
                        WHERE 1=1
                        AND `ttl` != 0
                        AND `creation` < date_sub(now(), interval 1 month)
                        AND NOW() > date_add(a.`creation`, interval + `ttl` minute)
                    ;";
                    $params->typeSQL = OdaLibBd::SQL_SCRIPT;
                    $retour = $this->BD_ENGINE->reqODASQL($params);
                    $obj_purge->nb = $retour->nombre;
                    $obj_purge->statut = "done";
                }
                $array_purges[] = $obj_purge;

                //--------------------------------------------------------------------------
                // Purge api_tab_transaction
                $obj_purge = new stdClass();
                $obj_purge->table = "api_tab_transaction";
                $obj_purge->nb = 0;
                $obj_purge->statut = "none";
                if($nb_api_tab_transaction > 1000){
                    $obj_purge->statut = "init";

                    $params = new OdaPrepareReqSql();
                    $params->sql = "DELETE
                        FROM `api_tab_transaction`
                        WHERE 1=1
                        AND `start` < date_sub(now(), interval 7 day)
                    ;";
                    $params->typeSQL = OdaLibBd::SQL_SCRIPT;
                    $retour = $this->BD_ENGINE->reqODASQL($params);
                    $obj_purge->nb = $retour->nombre;
                    $obj_purge->statut = "done";
                }
                $array_purges[] = $obj_purge;

                //--------------------------------------------------------------------------
                // Purge api_tab_log
                $obj_purge = new stdClass();
                $obj_purge->table = "api_tab_log";
                $obj_purge->nb = 0;
                $obj_purge->statut = "none";
                if($nb_api_tab_log > 1000){
                    $obj_purge->statut = "init";

                    $params = new OdaPrepareReqSql();
                    $params->sql = "DELETE
                        FROM `api_tab_log`
                        WHERE 1=1
                        AND `moment` < date_sub(now(), interval 7 day)
                    ;";
                    $params->typeSQL = OdaLibBd::SQL_SCRIPT;
                    $retour = $this->BD_ENGINE->reqODASQL($params);
                    $obj_purge->nb = $retour->nombre;
                    $obj_purge->statut = "done";
                }
                $array_purges[] = $obj_purge;
            }
            //--------------------------------------------------------------------------
            $params = new stdClass();
            $params->label = "purges";
            $params->value = $array_purges;
            $this->addDataStr($params);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    
    /**
      */
    function getReportInterfacMetric(){
        try {
            $params = new OdaPrepareReqSql();
            $params->sql = "SELECT REPLACE(`type`,SUBSTRING_INDEX(`type`, '/', 4), '') as 'interface' 
                ,COUNT(`id`) as 'nb'
                ,COUNT(`id`) *  AVG(TIMEDIFF(`end`,`start`)) as 'cost'
                ,AVG(TIMEDIFF(`end`,`start`)) as 'average'
                ,MAX(TIMEDIFF(`end`,`start`)) as 'maxTime'
                ,MIN(TIMEDIFF(`end`,`start`)) as 'minTime'
                FROM  `api_tab_transaction` 
                WHERE 1=1
                AND `end` != '0000-00-00 00:00:00'
                AND `end` > NOW() - INTERVAL 7 DAY
                GROUP BY `type`
            ;";
            $params->typeSQL = OdaLibBd::SQL_GET_ALL;
            $retour = $this->BD_ENGINE->reqODASQL($params);
            $this->addDataObject($retour->data->data);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    
    /**
     */
    function createLog(){
        try {
            $id = $this->BD_ENGINE->logTrace($this->inputs["type"], $this->inputs["msg"]);
            $this->addDataStr($id);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    
    /**
     */
    function getParam($key){
        try {
            $params = new stdClass();
            $params->nameObj = "api_tab_parameter";
            $params->keyObj = ["name" => $key];
            $retour = $this->BD_ENGINE->getSingleObject($params);
            $this->addDataObject($retour);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    
    /**
      */
    function setParam($key) {
        try {
            $params = new stdClass();
            $params->nameObj = "api_tab_parameter";
            $params->keyObj = ["name" => $key];
            $params->setObj = ["value" => $this->inputs["value"]];
            $id = $this->BD_ENGINE->setSingleObj($params);

            $params = new stdClass();
            $this->addDataStr($id);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    
    /**
      */
    function mail() {
        try {
            $params = new stdClass();
            $params->from = $this->inputs["from"];
            $params->fromLabel = $this->inputs["fromLabel"];
            $params->to = $this->inputs["to"];
            $params->subject = $this->inputs["subject"];
            $params->bodyText = $this->inputs["bodyText"];
            $params->bodyHtml = $this->inputs["bodyHtml"];
            $params->reply = $this->inputs["reply"];
            $params->replyLabel = $this->inputs["replyLabel"];
            $params->cc = $this->inputs["cc"];
            $params->cci = $this->inputs["cci"];
            $response = OdaLib::mail($params);
            $this->addDataObject($response);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
}