<?php
namespace Oda;
use \Exception
 , Oda\SimpleObject\OdaUser
 , Slim\Http\Request
 , Slim\Slim
 , \stdClass;
/**
 * LIBODA Librairy - main class
 *
 * Tool
 *
 * @author  Fabrice Rosito <rosito.fabrice@gmail.com>
 * @version 0.150127
 */
class OdaLibInterface {
    const STATE_INIT = 001;
    const STATE_CONSTRUCT = 002;
    const STATE_READY = 003;
    const STATE_ERROR = 004;
    const STATE_ERROR_AUTH = 006;
    const STATE_FINISH = 005;
    const METHOD_GET = "GET";
    const METHOD_PUT = "PUT";
    const METHOD_POST = "POST";
    const METHOD_HEAD = "HEAD";
    const METHOD_DELETE = "DELETE";
    const METHOD_OPTIONS = "OPTIONS";

    /**
     * Content of config.php object $OdaConfig
     *
     * @var OdaConfig
     */
    protected static $config;
    /**
     * All details of the interface
     *
     * @var OdaPrepareInterface
     */
    protected $params;
    /**
     * The bd engine
     *
     * @var OdaLibBd
     */
    public $BD_ENGINE;
    /**
     * The auth engine
     *
     * @var OdaLibBd
     */
    protected $BD_AUTH;
    /**
     * The Oda Key
     *
     * @var string
     */
    protected $keyAuth;
    /**
     * The details of output
     *
     * @var OdaRetourInterface
     */
    protected $object_retour;
    /**
     * The url for template
     *
     * @var string
     */
    public $urlTest;
    /**
     * The list of inputs
     *
     * @var array
     */
    public $inputs = array();
    /**
     * Mode debug, false by default
     *
     * @var boolean
     */
    public $modeDebug = false;
    /**
     * Public ou privé (clé obligatoire), true by default
     *
     * @var boolean
     */
    public $modePublic = true;
    /**
     * Mode de sortie text,json,xml,csv, json by default
     *
     * Pour le xml et csv, data doit contenir qu'un est unique array.
     *
     * @var string
     */
    public $modeSortie = "json";
    /**
     * The name of file output
     *
     * @var string
     */
    public $fileName = "";
    /**
     * @var int
     */
    protected $startMicro = 0;
    /**
     * The methode of the caller
     * @var string
     */
    public $callerMethode;
    /**
     * @var object
     */
    public $user;
    /**
     * name of the interface (after the api/)
     * @example getInfo.php of in rest user/1
     * @var string
     */
    public $name;
    /**
     * @var Int
     */
    private $transactionRecord;
    /**
     * For inherit right from rank and route
     * @example admin
     * @var String
     */
    private $inheritRightRoute;

    /**
     * @param type \Oda\OdaPrepareInterface
     * @return \Oda\OdaLibInterface
     */
    public function __construct(SimpleObject\OdaPrepareInterface $params){
        try {
            $this->params = $params;
            $this->modeDebug = $params->modeDebug;
            $this->modePublic = $params->modePublic;
            $this->inheritRightRoute = $params->inheritRightRoute;
            $this->modeSortie = $params->modeSortie;
            $this->fileName = $params->fileName;
            $this->object_retour = new SimpleObject\OdaRetourInterface();
            $this->callerMethode = $_SERVER['REQUEST_METHOD'];

            self::$config = SimpleObject\OdaConfig::getInstance();

            self::$config ->isOK();

            $params_bd = new stdClass();
            $params_bd->bd_conf = self::$config->BD_ENGINE;
            $params_bd->modeDebug = $this->modeDebug;
            $this->BD_ENGINE = new OdaLibBd($params_bd);

            if(is_null(self::$config->BD_AUTH->base)){
                $this->BD_AUTH = $this->BD_ENGINE;
            }else{
                $params_bd = new stdClass();
                $params_bd->bd_conf = self::$config->BD_AUTH;
                $params_bd->modeDebug = $this->modeDebug;
                $this->BD_AUTH = new OdaLibBd($params_bd);
            }

            $this->object_retour->statut = self::STATE_CONSTRUCT;

            $this->inputs = $this->recupInputs($params->arrayInput, $params->arrayInputOpt);

            $start = new SimpleObject\OdaDate();
            $this->startMicro = SimpleObject\OdaDate::getMicro();
            $this->object_retour->metrics["start"] = $start->getDateTimeWithMili();

            $this->transactionRecord = $this->getParameter('transaction_record');

            $this->_initTransaction($this->inputs, $start);

            $this->checkKey();

            $this->object_retour->statut = self::STATE_READY;

            return $this;
        } catch (Exception $ex){
            $this->dieInError($ex.'');
        }
    }
    /**
     * Destructor
     *
     * @access public
     * @return null
     */
    public function __destruct(){
        if(($this->object_retour->statut != self::STATE_ERROR)
            && ($this->object_retour->statut != self::STATE_ERROR_AUTH)
        ){
            $this->object_retour->statut = self::STATE_FINISH;
        }
        $strSorti = "";

        $end = new SimpleObject\OdaDate();
        $this->object_retour->metrics["end"] = $end->getDateTimeWithMili();

        $endMicro = SimpleObject\OdaDate::getMicro();

        $duration = $endMicro - $this->startMicro;
        $this->object_retour->metrics["duration"] = $duration;

        if($this->object_retour->id_transaction === 0){
            unset($this->object_retour->id_transaction);
        }

        //choix du traitement
        switch ($this->modeSortie) {
            case "text":
                $strSorti = OdaLib::fomatage_text($this->object_retour);
                break;
            case "json":
                $strSorti = OdaLib::fomatage_json($this->object_retour);
                break;
            case "xml":
                $strSorti = OdaLib::fomatage_xml($this->object_retour->data);
                break;
            case "csv":
                $strSorti = OdaLib::fomatage_csv($this->object_retour->data);
                break;
            default:
                $strSorti = OdaLib::fomatage_text($this->object_retour);
                break;
        }

        if( (!is_null($this->BD_ENGINE)) && ($this->transactionRecord === 1) && (isset($this->object_retour->id_transaction)) && ($this->object_retour->id_transaction !== 0) ){
            $this->_finishTransaction($strSorti, $end);
        }

        if(!empty($this->fileName)&&(!empty($this->modeSortie))){
            header("Content-type: text/".$this->modeSortie."; charset=utf-8");
            header("Content-Disposition: attachment; filename=".$this->fileName.".".$this->modeSortie);
            header("Pragma: no-cache");
            header("Expires: 0");
            echo "\xEF\xBB\xBF"; // UTF-8 BOM
            echo $strSorti;
        } elseif($this->modeSortie !== "raw") {
            echo $strSorti;
        }
    }
    /*
     * @return array
     */
    protected function recupInputs($p_arrayIn, $p_arrayInOpt=array()) {
        try {
            $arrayOut = array ();
            $strError = null;

            //recup les gets
            $arrayGetAll = array ();
            if(isset($_GET)) {
                foreach($_GET as $key=>$val) {
                    $arrayGetAll[$key] = $val;
                }
            }

            //recup les posts
            $arrayPostAll = array ();
            if(isset($_POST)) {
                foreach($_POST as $key=>$val) {
                    $arrayPostAll[$key] = $val;
                }
            }

            //request-payload
            $request_body = file_get_contents('php://input');
            $arrayIntput = json_decode($request_body, true);

            //crypt
            if(isset($arrayGetAll["cryptODA"]) && isset($arrayGetAll["datas"])){
                $arrayGetAll = OdaLib::uncryptODA($arrayGetAll["datas"]);
            }

            if(isset($arrayPostAll["cryptODA"]) && isset($arrayPostAll["datas"])){
                $arrayPostAll = OdaLib::uncryptODA($arrayPostAll["datas"]);
            }

            if(isset($arrayIntput["cryptODA"]) && isset($arrayIntput["datas"])){
                $arrayPostAll = OdaLib::uncryptODA($arrayPostAll["datas"]);
            }

            //On init les inputs du framework
            if(isset($arrayPostAll["keyAuthODA"])){
                $arrayOut["keyAuthODA"] = $arrayPostAll["keyAuthODA"];
            }else if(isset($arrayGetAll["keyAuthODA"])){
                $arrayOut["keyAuthODA"] = $arrayGetAll["keyAuthODA"];
            }else if(isset($arrayIntput["keyAuthODA"])){
                $arrayOut["keyAuthODA"] = $arrayIntput["keyAuthODA"];
            }else{
                $arrayOut["keyAuthODA"] = null;
            }

            if(isset($arrayPostAll["ODAFileType"])){
                $arrayOut["ODAFileType"] = $arrayPostAll["ODAFileType"];
            }else if(isset($arrayGetAll["ODAFileType"])){
                $arrayOut["ODAFileType"] = $arrayGetAll["ODAFileType"];
            }else if(isset($arrayIntput["ODAFileType"])){
                $arrayOut["keyAuthODA"] = $arrayIntput["ODAFileType"];
            }else{
                $arrayOut["ODAFileType"] = null;
            }

            if(isset($arrayPostAll["ODAFileName"])){
                $arrayOut["ODAFileName"] = $arrayPostAll["ODAFileName"];
            }else if(isset($arrayGetAll["ODAFileName"])){
                $arrayOut["ODAFileName"] = $arrayGetAll["ODAFileName"];
            }else if(isset($arrayIntput["ODAFileName"])){
                $arrayOut["keyAuthODA"] = $arrayIntput["ODAFileName"];
            }else{
                $arrayOut["ODAFileName"] = null;
            }

            //on recupére les inputs déclarés en option
            foreach($p_arrayInOpt as $key=>$val) {
                if(isset($arrayPostAll[$key])){
                    $arrayOut[$key] = $arrayPostAll[$key];
                }else if(isset($arrayGetAll[$key])){
                    $arrayOut[$key] = $arrayGetAll[$key];
                }else if(isset($arrayIntput[$key])){
                    $arrayOut[$key] = $arrayIntput[$key];
                }else{
                    $arrayOut[$key] = $val;
                }
            }

            //on recupére les inputs déclarés
            foreach($p_arrayIn as $val) {
                if(isset($arrayPostAll[$val])){
                    $arrayOut[$val] = $arrayPostAll[$val];
                }else if(isset($arrayGetAll[$val])){
                    $arrayOut[$val] = $arrayGetAll[$val];
                } else if(isset($arrayIntput[$val])){
                    $arrayOut[$val] = $arrayIntput[$val];
                } else {
                    $arrayOut[$val] = null;
                    $strError .= $val.", ";
                }
            }

            if(!empty($arrayOut["ODAFileName"])){
                $this->fileName = $arrayOut["ODAFileName"];
            }

            if(!empty($arrayOut["ODAFileType"])){
                $this->modeSortie = $arrayOut["ODAFileType"];
            }

            if(!empty($arrayOut["keyAuthODA"])){
                $this->keyAuth = $arrayOut["keyAuthODA"];
            }

            //Erreur dans les inputs obligatoire
            if($strError != null){
                $this->inputs = $arrayOut;
                $params = new stdClass();
                $params->class = __CLASS__;
                $params->function = __FUNCTION__;
                $params->message = "Field(s) missing : ".$strError.'ex : '.$this->getUrlTest();
                throw new SimpleObject\OdaException($params);
            }

            return $arrayOut;
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /*
     * @return string
     */
    public function getUrlTest(){
        try {
            $SCRIPT_NAME = $_SERVER["SCRIPT_NAME"];
            $tabName = explode("/",$SCRIPT_NAME);
            $SCRIPT_NAME = str_replace("/".$tabName[1]."/", "", $SCRIPT_NAME);

            $strUrl = self::$config->urlServer.$SCRIPT_NAME."?milis=". SimpleObject\OdaDate::getMicro();

            foreach ($this->inputs as $key => $value){
                if(($key != 'keyAuthODA')&&($key != 'ODAFileName')&&($key != 'ODAFileType')){
                    if(is_null($value)||($value == '')){
                        $value = 'xx';
                    }
                    $strUrl .= "&" . $key . "=" . $value;
                }
            }

            return $strUrl;
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     * To add an object in data output
     *
     * $p_params :
     * - OdaRetourReqSql retourSql
     * - string label
     *
     * @param stdClass p_params
     */
    public function addDataReqSQL($p_params){
        try {
            if($p_params->retourSql->strStatut != OdaLibBd::SQL_STATUT_FINISH_OK){
                $this->object_retour->strErreur = $p_params->retourSql->strErreur;
                $this->object_retour->statut = self::STATE_ERROR;
                die();
            }else{
                if(isset($p_params->label)){
                    if(!is_array($this->object_retour->data)){
                        $this->object_retour->data = array($p_params->label => $p_params->retourSql->data);
                    }else{
                        $this->object_retour->data[$p_params->label] = $p_params->retourSql->data;
                    }
                }else{
                    $this->object_retour->data = $p_params->retourSql->data;
                }
            }
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }

    /**
     * To add an object in data output
     *
     * $p_params :
     * - string value
     * - string label
     *
     * @param stdClass p_params
     */
    public function addDataObject($p_params){
        try {
            if(isset($p_params->label)){
                if(!is_array($this->object_retour->data)){
                    $this->object_retour->data = array($p_params->label => $p_params->value);
                }else{
                    $this->object_retour->data[$p_params->label] = $p_params->value;
                }
            }else{
                $this->object_retour->data = $p_params;
            }
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     * Attetion, not handle error from the OdaRetourSQL.
     *
     * $p_params :
     * - string value
     * - string label
     *
     * or
     *
     * $p_params  = string
     *
     * @param stdClass|string  p_params
     */
    public function addDataStr($p_params){
        try {
            if(is_object($p_params)){
                if(is_object($p_params->value)){
                    $this->object_retour->strErreur = "The value is not a string.";
                    $this->object_retour->statut = self::STATE_ERROR;
                    die();
                }

                if(isset($p_params->label)){
                    if(!is_array($this->object_retour->data)){
                        $this->object_retour->data = array($p_params->label => $p_params->value);
                    }else{
                        $this->object_retour->data[$p_params->label] = $p_params->value;
                    }
                }else{
                    $this->object_retour->data = $p_params->value;
                }
            }else{
                $this->object_retour->data = $p_params;
            }
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     * initTransaction
     * @return int
     */
    protected function _initTransaction ($inputs, $debut) {
        try {
            if($this->transactionRecord == 1){
                $params = new SimpleObject\OdaPrepareReqSql();
                $params->sql = "INSERT INTO  `api_tab_transaction` (
                        `id` ,
                        `type` ,
                        `status` ,
                        `input` ,
                        `output` ,
                        `start` ,
                        `end`
                    )
                    VALUES (
                        NULL ,  :type,  'start',  :input,  '',  :start,  ''
                    )
                ;";
                $strJsonInput = OdaLib::fomatage_json($inputs);
                $params->typeSQL = OdaLibBd::SQL_INSERT_ONE;
                $params->bindsValue = [
                    "type" => [ "value" => $_SERVER["SCRIPT_FILENAME"]]
                    , "input" => [ "value" => $strJsonInput]
                    , "start" => [ "value" => $debut->getDateTimeWithMili()]
                ];
                $retour = $this->BD_ENGINE->reqODASQL($params);
                $this->object_retour->id_transaction = $retour->data;
            }else{
                $this->object_retour->id_transaction = 0;
            }

        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     * finishTransaction
     */
    protected function _finishTransaction($str, $end){
        try {
            $params = new SimpleObject\OdaPrepareReqSql();
            $params->sql = "UPDATE `api_tab_transaction`
                SET `output` = :strSort
                    , `status` = 'output'
                    , `end` = :end
                WHERE `id` = :idTransaction
            ;";
            $params->typeSQL = OdaLibBd::SQL_SCRIPT;
            $params->bindsValue = [
                "idTransaction" => [ "value" => $this->object_retour->id_transaction]
                , "strSort" => [ "value" => $str]
                , "end" => [ "value" => $end->getDateTimeWithMili()]
            ];
            $req = $this->BD_ENGINE->reqODASQL($params);
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }
    /**
     * checkKey
     */
    public function checkKey(){
        try {
            $ajour = false;
            $keyValid = false;

            if($this->keyAuth !== '') {
                $params = new SimpleObject\OdaPrepareReqSql();
                $params->sql = "SELECT `id`, `key`, `datas`, `signature`
                        , IF(a.`ttl` = 0, TRUE, IF(((a.`creation` + INTERVAL a.`ttl` MINUTE) < NOW()), FALSE, TRUE)) as 'ajour'
                        FROM `api_tab_session` a
                        WHERE 1=1
                        AND a.`key` = '" . $this->keyAuth . "'
                    ;";
                $params->typeSQL = OdaLibBd::SQL_GET_ONE;
                $retour = $this->BD_AUTH->reqODASQL($params);

                if($retour->data){
                    $keyValid = true;
                    $ajour = $retour->data->ajour;
                    
                    $codeUser = json_decode($retour->data->datas, true)['code_user'];
                    $signature = json_decode($retour->data->signature);

                    //check signature
                    if(self::$config->security->referer === true && isset($signature->referer)){
                        if($this->slim->request->headers->get('HTTP_REFERER') !== $signature->referer){
                            $this->dieInError('Origin not allowed for this session', self::STATE_ERROR_AUTH);
                        }
                    }

                    $this->user = new OdaUser($codeUser);

                    $params = new SimpleObject\OdaPrepareReqSql();
                    $params->sql = "SELECT a.`id`, a.`active`, a.`active` as 'actif', 
                        b.`value` as 'rank_value', b.`value` as 'indice', b.`id` as 'rank_id', b.`id` AS 'rankId'
                        FROM `api_tab_user` a, `api_tab_rank` b
                        WHERE 1=1
                        AND a.`rank_id` = b.`id`
                        AND a.`code` = :code_user
                    ;";
                    $params->bindsValue = [
                        "code_user" => $codeUser
                    ];
                    $params->typeSQL = OdaLibBd::SQL_GET_ONE;
                    $retour = $this->BD_ENGINE->reqODASQL($params);
                    
                    if($retour->data){
                        $this->user->id = $retour->data->id;
                        $this->user->rankId = $retour->data->rankId;
                        $this->user->indice = $retour->data->indice;
                        $this->user->active = $retour->data->actif;
                    }else{
                        $this->dieInError('No user for this key', self::STATE_ERROR_AUTH);
                    }
                }
            }

            if(!$this->modePublic){
                if($this->keyAuth == '') {
                    $this->dieInError('Key auth empty.', self::STATE_ERROR_AUTH);
                }else{
                    if(!$keyValid){
                        $this->dieInError('Key auth invalid.', self::STATE_ERROR_AUTH);
                    }else{
                        if(!$ajour){
                            $this->dieInError('Session expired.', self::STATE_ERROR_AUTH);
                        }else{
                            $params = new SimpleObject\OdaPrepareReqSql();
                            $params->sql = "SELECT b.`value`, a.`open`
                                FROM `api_tab_api_rank` a, `api_tab_rank` b
                                WHERE 1=1
                                AND a.`rank_id` = b.`id`
                                AND a.`interface` = :interface
                                AND a.`method` = :method
                            ;";

                            $params->bindsValue = [
                                "interface" => Slim::getInstance()->router()->getCurrentRoute()->getPattern(),
                                "method" => $this->params->slim->request->getMethod()
                            ];
                            $params->typeSQL = OdaLibBd::SQL_GET_ONE;
                            $retour = $this->BD_ENGINE->reqODASQL($params);

                            //Get acces if the user value is lower than value need
                            //Ex if user value is 30 (user) and the rule value is 40 (respon), the acces is denied
                            if(($retour->data) && (!$retour->data->open) && ($retour->data->value <= $this->user->value)){
                                $this->dieInError('Value ranking user not enough.', self::STATE_ERROR_AUTH);
                            }else{
                                if($this->inheritRightRoute != ""){
                                    //Check if the user rank can access to route
                                    $params = new SimpleObject\OdaPrepareReqSql();
                                    $params->sql = "SELECT a.`id`, b.`menu_ids`, b.`menu_ids` like concat('%;',a.`id`,';%') as 'present'
                                        FROM `api_tab_menu` a, `api_tab_menu_rank` b
                                        WHERE 1=1
                                        AND a.`route` = :inheritRightRoute
                                        AND b.`rank_id` = :rankId
                                    ;";
                                    $params->bindsValue = [
                                        "inheritRightRoute" => $this->inheritRightRoute,
                                        "rankId" => $this->user->rankId
                                    ];
                                    $params->typeSQL = OdaLibBd::SQL_GET_ONE;
                                    $retour = $this->BD_ENGINE->reqODASQL($params);

                                    if(($retour->data) && (!$retour->data->present)){
                                        $this->dieInError("Affiliated route:'".$this->inheritRightRoute."' not allowed for user:'".$this->user->codeUser."'.", self::STATE_ERROR_AUTH);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch ( Exception $ex ) {
            $this->dieInError($ex.'');
        }
    }
    /**
     * checkSession
     * @param array $p_params
     * @return boolean
     */
    public function checkSession($p_params){
        try {
            $params = new SimpleObject\OdaPrepareReqSql();
            $params->sql = "SELECT IF(a.`ttl` = 0, true, (a.`creation` + INTERVAL a.`ttl` MINUTE) > NOW()) as 'check'
                FROM `api_tab_session` a
                WHERE 1=1
                AND a.`key` = :key
            ;";
            $params->bindsValue = [
                "key" => $p_params["key"]
            ];
            $params->typeSQL = OdaLibBd::SQL_GET_ONE;
            $retour = $this->BD_AUTH->reqODASQL($params);

            return $retour->nombre;
        } catch ( Exception $ex ) {
            $this->dieInError($ex.'');
        }
    }
    /**
     * deleteSession
     * @param string $p_params
     * @return boolean
     */
    public function deleteSession($p_params){
        try {
            $params = new SimpleObject\OdaPrepareReqSql();
            $params->sql = "DELETE
                FROM `api_tab_session`
                WHERE 1=1
                AND `key` = :key
            ;";
            $params->bindsValue = [
                "key" => $p_params
            ];
            $params->typeSQL = OdaLibBd::SQL_SCRIPT;
            $retour = $this->BD_AUTH->reqODASQL($params);

            return $retour->nombre;
        } catch ( Exception $ex ) {
            $this->dieInError($ex.'');
        }
    }
    /**
     * buildSession
     * @param array $p_params
     * @param array $p_params[code_user]
     * @param array $p_params[password]
     * @param array $p_params[dbPassword]
     * @param array $p_params[sessionTimeOutMinute]
     * @return string
     */
    public function buildSession($p_params){
        try {
            $v_code_user = $p_params["code_user"];
            $v_key = "";

            //Detruit les veilles clés
            $params = new SimpleObject\OdaPrepareReqSql();
            $params->sql = "DELETE FROM `api_tab_session`
                WHERE 1=1
                AND `datas` like '%\"code_user\":\"".$v_code_user."\"%'
                AND (`creation` + INTERVAL `ttl` MINUTE) < NOW()
                AND `ttl` != 0
            ;";
            $params->typeSQL = OdaLibBd::SQL_SCRIPT;
            $retour = $this->BD_AUTH->reqODASQL($params);

            //Check log pass
            $checkPass = true;
            if(!OdaLib::startsWith($p_params["password"], "authByGoogle-")){
                $checkPass = password_verify($p_params["password"], $p_params['dbPassword']);
            }
            
            if($checkPass){
                //Vérifie la présence d'une clé
                $params = new SimpleObject\OdaPrepareReqSql();
                $params->sql = "SELECT a.`id`, a.`key`
                    FROM `api_tab_session` a
                    WHERE 1=1
                    AND a.`datas` like '%\"code_user\":\"".$v_code_user."\"%'
                    AND (a.`creation` + INTERVAL a.`ttl` MINUTE) > NOW()
                ;";
                $params->typeSQL = OdaLibBd::SQL_GET_ONE;
                $retour = $this->BD_AUTH->reqODASQL($params);

                if($retour->data){
                    $v_key = $retour->data->key;
                }else {
                    $params = new stdClass();
                    $params->code = $v_code_user;
                    $params->ttl = $p_params['sessionTimeOutMinute'];
                    $v_key = self::_createSession($this, $params);
                }
            }else {
                $this->dieInError('oda-main.auth-fail-password-wrong', self::STATE_ERROR_AUTH);
            }

            return $v_key;
        } catch ( Exception $ex ) {
            $this->object_retour->strErreur = $ex.'';
            $this->object_retour->statut = self::STATE_ERROR;
            die();
        }
    }
    /**
     * @name getParameter
     * @param string $p_parameterName
     * @return string|int $parameterValue
     */
    public function getParameter($p_parameterName) {
        try {
            $parameterValue = null;

            $params = new stdClass();
            $params->nameObj = "api_tab_parameter";
            $params->keyObj = ["name" => $p_parameterName];
            $retour = $this->BD_ENGINE->getSingleObject($params);

            if($retour){
                switch ($retour->type) {
                    case "varchar":
                        $parameterValue = $retour->value;
                        break;
                    case "int":
                        $parameterValue = (int) $retour->value;
                        break;
                    default:
                        $parameterValue = $retour->value;
                        break;
                }
            }

            return $parameterValue;
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }

    /**
     * @name setParameter
     * @desc met à jour la valeur du param
     * @return boolean
     */
    function setParameter($p_param, $p_valeur) {
        try {
            $params = new \stdClass();
            $params->nameObj = "api_tab_parameter";
            $params->keyObj = ["name" => $p_param];
            $params->setObj = ["value" => $p_valeur];
            $id = $this->BD_ENGINE->setSingleObj($params);

            return $id;
        } catch (Exception $ex) {
            $this->dieInError($ex.'');
        }
    }

    /**
     * To die an interface
     *
     * @param String $message
     */
    public function dieInError($message, $errorCode = self::STATE_ERROR){
        $this->object_retour->strErreur = $message;
        $this->object_retour->statut = $errorCode;
        die();
    }

    /**
     * create a session et return token
     * @param $p->code
     * @param $p->ttl
     */
    static function _createSession($instance, $p){
        try {
            //build token
            $token = self::_createToken($instance, $p);

            $signature = array(
                "host" => $instance->slim->request()->getHost(),
                "referer" => $instance->slim->request->headers->get('HTTP_REFERER'),
                "userAgent" => $instance->slim->request()->getUserAgent()
            );
            $signature = \json_encode($signature);

            $params = new SimpleObject\OdaPrepareReqSql();
            $params->sql = "INSERT INTO `api_tab_session`(
                    `key`,
                    `datas`,
                    `signature`,
                    `creation`,
                    `ttl`
                )
                VALUES (
                    :key, :datas, :signature, NOW(), :ttl
                )
            ;";
            $params->bindsValue = [
                "key" => $token->key,
                "datas" => $token->datas,
                "signature" => $signature,
                "ttl" => $p->ttl
            ];
            $params->typeSQL = OdaLibBd::SQL_INSERT_ONE;
            $result = $instance->BD_ENGINE->reqODASQL($params);
            if(!$result->data){
                return false;
            }

            return $token->key;
        } catch (Exception $ex) {
            $instance->dieInError($ex.'');
        }
    }

    /**
     * create a session et return token
     * @param $p->code
     */
    static function _createToken($instance, $p){
        try {
            $response = new stdClass();

            //build token
            $strDate = \date('YmdHis');
            $response->key = \md5($p->code."_".$strDate);

            $json = new stdClass();
            $json->code_user = $p->code;
            $json->date = $strDate;
            $response->datas = \json_encode($json);

            return $response;
        } catch (Exception $ex) {
            $instance->dieInError($ex.'');
        }
    }
}