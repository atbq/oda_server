<?php
namespace Oda;

use 
    stdClass
;

/**
 * @author  Fabrice Rosito <rosito.fabrice@atbq.fr>
 * @version 0.180207
 */
class OdaTemplate {

    protected $file;
    protected $values = array();

    /**
     * class constructor
     *
     * @param stdClass $p_params
     * @return OdaDate $this
     */
    public function __construct($file){
        $this->file = $file;
    }
    /**
     * Destructor
     *
     * @access public
     * @return null
     */
    public function __destruct(){
        
    }

    /**
     * @access public
     * @return null
     */
    public function set($key, $value) {
        try{
            $this->values[$key] = $value;
        } catch (Exception $e) {
            $msg = $e->getMessage();
            Throw new \Exception('Erreur dans '.__CLASS__.' : '.$msg);
            return null;
        }
    }
      
    /**
     * @access public
     * @return String
     */
    public function output() {
        try{
            if (!file_exists($this->file)) {
                return "Error loading template file ($this->file).";
            }
            $output = file_get_contents($this->file);
        
            foreach ($this->values as $key => $value) {
                $tagToReplace = "[@$key]";
                $output = str_replace($tagToReplace, $value, $output);
            }
        
            return $output;
        } catch (Exception $e) {
            $msg = $e->getMessage();
            Throw new \Exception('Erreur dans '.__CLASS__.' : '.$msg);
            return null;
        }
    }

    /**
     * @access public
     * @return String
     */
    static public function merge($templates, $separator = "n") {
        try{
            $output = "";
        
            foreach ($templates as $template) {
                $content = (get_class($template) !== "Oda\OdaTemplate")
                    ? "Error, incorrect type - expected OdaTemplate. (".get_class($template).")"
                    : $template->output();
                $output .= $content . $separator;
            }
        
            return $output;
        } catch (Exception $e) {
            $msg = $e->getMessage();
            Throw new \Exception('Erreur dans '.__CLASS__.' : '.$msg);
            return null;
        }
    }
}