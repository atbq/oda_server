<?php

namespace Oda;

use 
    stdClass,
    Exception,
    OdaTemplate, 
    Aws\Ses\SesClient,
    Oda\SimpleObject\OdaConfig,
    PHPMailer\PHPMailer\PHPMailer,
    Aws\SesException\SesException
;

/**
 * @author  Fabrice Rosito <rosito.fabrice@atbq.fr>
 * @version 180220
 */
class OdaLib {
    /**
     * class constructor
     *
     * @param stdClass $p_params
     * @return OdaDate $this
     */
    public function __construct($p_params = NULL){}

    /**
     * Destructor
     *
     * @access public
     * @return null
     */
    public function __destruct(){}

    /**
    * @name cryptODA
    * @param Array datas
    * @return String
    */
    static function cryptODA($datas) {
        try {
            $datas = json_encode($datas);
            
            $hex = '';
            for ($i = 0; $i < strlen($datas); $i++) {
                $char = substr($datas, $i, 1);
                $pos = ord($char);
                $hex .= dechex($pos);
            }
            return $hex;
        } catch (Exception $e) {
            $msg = $e->getMessage();
            Throw new Exception('Erreur dans '.__CLASS__.' : '.$msg);
            return null;
        }
    }

    /**
    * @name uncryptODA
    * @param String datas
    * @param Boolean isJson
    * @return String | Array
    */
    static function uncryptODA($datas, $isJson = true) {
        try {
            $datasUnCrypt = '';
            for ($i = 0; $i < strlen($datas); $i=$i+2) {
                $hex = substr($datas, $i, 2);
                if($hex !== ""){
                    $code = intval($hex, 16);
                    //echo($code . "=>");
                    $char = html_entity_decode('&#'.$code.';',ENT_NOQUOTES,'UTF-8');
                    if(substr($char, 0, 1) === '&'){
                        $char = chr($code);
                    }
                    //echo($char . PHP_EOL);
                    $datasUnCrypt .= $char;
                }
            }

            //echo($datasUnCrypt . PHP_EOL);

            if(!$isJson){
                return $datasUnCrypt;
            }else{
                try{
                    $output = json_decode($datasUnCrypt, true);
                    if($output === null){
                        Throw new Exception('not good');
                    }
                    return $output;
                } catch (Exception $e) {
                    $output = str_replace("'", '"', $datasUnCrypt);
                    $output = json_decode($output, true);
                    return $output;
                }
            }
        } catch (Exception $e) {
            $msg = $e->getMessage();
            Throw new Exception('Erreur dans '.__CLASS__.' : '.$msg);
            return null;
        }
    }

    /**
    * @name fomatage_json
    * @param class $p_object_retour
    * @return string
    */
    static function fomatage_json($p_object) {
       try {
           $output = "";
           //$object_retour est traduit dans son ensemble
           if(isset($p_object)) {
               $resultats_json = json_encode($p_object);
               $resultats_json = str_replace('\/', "/", $resultats_json);
               $output = $resultats_json;
           }

           return $output;
       } catch (Exception $e) {
           $msg = $e->getMessage();
           Throw new Exception('Erreur dans '.__CLASS__.' : '.$msg);
           return null;
       }
    }

    /**
     * @name fomatage_text
     * @param string $p_strSorti
     * @return string
     */
    static function fomatage_text($p_strSorti) {
        try {
            //$strSorti à valeur d'origine
            return $p_strSorti;
        } catch (Exception $e) {
           $msg = $e->getMessage();
           Throw new Exception('Erreur dans '.__CLASS__.' : '.$msg);
           return null;
        }
    }

    /**
     * @name fomatage_xml
     * @param class $p_object_retour
     * @return string
     */
    static function fomatage_xml($p_object) {
        try {
            $output = "";
            //$object_retour->data["resultat"]->data est traduit en xml, c'est un tableau en théorie
            if(isset($p_object)) {
                if(is_array($p_object)){
                    $output = self::generate_valid_xml_from_array($p_object);
                }
            }
            return $output;
        } catch (Exception $e) {
           $msg = $e->getMessage();
           Throw new Exception('Erreur dans '.__CLASS__.' : '.$msg);
           return null;
        }
    }

    /**
     * @name fomatage_csv
     * @param class $p_object_retour
     * @return string
     */
    static function fomatage_csv($p_object) {
        try {
            $output = "";
            //$object_retour->data["resultat"]->data est traduit en xml, cela doit être un tableau
            if(isset($p_object)) {
                if(is_array($p_object)){
                    $output = self::arrayToCsv($p_object);
                }
            }
            return $output;
        } catch (Exception $e) {
           $msg = $e->getMessage();
           Throw new Exception('Erreur dans '.__CLASS__.' : '.$msg);
           return null;
        }
    }

    /**
     * @name generate_valid_xml_from_array
     * @param array $array
     * @param string $node_block
     * @param string $node_name
     * @return string
     */
    static function generate_valid_xml_from_array($array, $node_block='nodes', $node_name='node') {
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>' . "\n";

        $xml .= '<' . $node_block . '>' . "\n";
        $xml .= self::generate_xml_from_array($array, $node_name);
        $xml .= '</' . $node_block . '>' . "\n";

        return $xml;
    }

    /**
     * @name generate_xml_from_array
     * @param array $array
     * @param string $node_name
     * @return string
     */
    static function generate_xml_from_array($array, $node_name) {
        $xml = '';

        if (is_array($array) || is_object($array)) {
            foreach ($array as $key=>$value) {
                if (is_numeric($key)) {
                        $key = $node_name;
                }

                $xml .= '<' . $key . '>' . self::generate_xml_from_array($value, $node_name) . '</' . $key . '>' . "\n";
            }
        } else {
            $xml =  htmlspecialchars($array, ENT_QUOTES);
        }

        return $xml;
    }

    /**
     * @name arrayToCsv
     * @param array $array
     * @return string
     */
    static function arrayToCsv($array) {
        $csv = '';
        if ((is_array($array) || is_object($array))&&(isset($array[0]))) {
            //entête
            foreach ($array[0] as $labelCol=>$value){
                if (is_numeric($labelCol)) {
                    $csv .= $labelCol .';';
                }else{
                    $val = htmlspecialchars($labelCol, ENT_QUOTES);
                    $csv .= '"'.$val .'";';
                }
            }
            $csv .= "\n";
            //data
            foreach($array as $key => $val) {
                foreach ($array[$key] as $labelCol=>$value) {
                    if (is_numeric($value)) {
                        $csv .= $value .';';
                    }else{
                        $val = htmlspecialchars($value, ENT_QUOTES);
                        $csv .= '"'.$val .'";';
                    }
                }
                $csv .= "\n";
            }
        }

        return $csv;
    }

    /**
     * @author Fabrice Rosito <rosito.fabrice@atbq.fr>
     * @param stdClass $params
     * @param String $params->from
     * @param String $params->to
     * @param String $params->cc
     * @param String $params->subject
     * @param String $params->bodyText
     * @param String $params->bodyHtml
     * @param String $params->attachement
     * @version 180220
     */
    static function sendMailSES($params) {
        try {
            $result = false;

            $config = SimpleObject\OdaConfig::getInstance();

            $region = (isset($config->AWS_SES->region) && $config->AWS_SES->region != null)?$config->AWS_SES->region:'eu-west-1';

            $clientMail = SesClient::factory(array(
                'version'=> 'latest',     
                'region' => $region
            ));

            $mail = new PHPMailer();

            if(strpos($params->to, ";")){
                $tos = explode(";", $params->to);
                foreach($tos as $to){
                    if($to !== ""){
                        $mail->addAddress($to);
                    }
                }
            }else{
                $mail->addAddress($params->to);
            }

            if(isset($params->cc) && $params->cc !== ""){
                if(strpos($params->cc, ";")){
                    $ccs = explode(";", $params->cc);
                    foreach($ccs as $cc){
                        if($cc !== ""){
                            $mail->AddCC($cc);
                        }
                    }
                }else{
                    $mail->AddCC($params->cc);
                }
            }

            if(isset($params->cci) && $params->cci !== ""){
                if(strpos($params->cci, ";")){
                    $ccis = explode(";", $params->cci);
                    foreach($ccis as $cci){
                        if($cci !== ""){
                            $mail->AddBCC($cci);
                        }
                    }
                }else{
                    $mail->AddBCC($params->cci);
                }
            }

            $mail->setFrom($params->from);
            $mail->Subject = $params->subject;
            $mail->CharSet = 'UTF-8';
            if(!isset($params->bodyText) || $params->bodyText == null){
                $mail->AltBody = 'Problem with html version.';
            }else{
                $mail->AltBody = $params->bodyText;
            }
            $mail->Body = $params->bodyHtml;
            $mail->isHTML(true);
            if(isset($params->attachement) && $params->attachement != null){
                $mail->addAttachment($params->attachement);
            }

            $mail->preSend();

            $row = $mail->getSentMIMEMessage();

            $args = [
                'RawMessage'   => [
                    'Data' => $row
                ]
            ];

            if(isset($config->fromArn)){
                $args += [ 'SourceArn' =>  $config->fromArn ];
                $args += [ 'FromArn' =>  $config->fromArn ];
            }

            $result = $clientMail->sendRawEmail($args);
            return $result;
        } catch (SesException $error) {
            $msg = "The email was not sent. Error message: ".$error->getAwsErrorMessage()."\n";
            Throw new Exception($msg);
        } catch (Exception $e) {
            $msg = "Function: " . __FUNCTION__ . ", error: " . $e->getMessage();
            Throw new Exception($msg);
        }
    }

    /**
     * @name sendMailGun
     * @return null|string
     * @throws Exception
     * @param sdtClass $p_params
     */
    static function sendMailGun($p_params) {
        try {
            $config = OdaConfig::getInstance();
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, 'api:'.$config->MAILGUN->api_key);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v2/'.$config->MAILGUN->domaine.'/messages');

            $params = array(
                'from' => $p_params["email_labelle_ori"].'<'.$p_params["email_mail_ori"].'>'
                ,'to' => $p_params["email_mails_dest"]
                ,'subject' => $p_params["sujet"]
                ,'html' => $p_params["message_html"]
                ,'text' => $p_params["message_txt"]
            );

            if(!is_null($p_params["email_mails_copy"])){
                $params['cc'] = $p_params["email_mails_copy"];
            }

            if(!is_null($p_params["email_mails_cache"])){
                $params['bcc'] = $p_params["email_mails_cache"];
            }

            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            $result = curl_exec($ch);
            curl_close($ch);
            
            $return = ($result)?"OK":"KO";

            return $return;
        } catch (Exception $e) {
            $msg = "Function: " . __FUNCTION__ . ", error: " . $e->getMessage();
            Throw new Exception($msg);
            return null;
       }
    }

    /**
     * @return null|string
     * @throws Exception
     * @param sdtClass $p_params
     */
    static function sendMail($p_params) {
        try {
            $strRetour = "ok";
            
            if (ini_get("SMTP") == "none") {
                $strRetour = "ko: smtp in localhost";
                goto gotoFinish;
            }

            if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $p_params["email_mails_dest"])){
                $passage_ligne = "\r\n";
            }else{
                $passage_ligne = "\n";
            }

            //=====Création de la boundary
            $boundary = "-----=".md5(rand());
            //==========

            //=====Création du header de l'e-mail.
            if(($p_params["email_mail_ori"] != null)&&($p_params["email_labelle_ori"] != null)){
                $header = "From: \"".$p_params["email_labelle_ori"]."\"<".$p_params["email_mail_ori"].">".$passage_ligne;
            }else{
                $header = "From: \"".$p_params["email_mail_ori"]."\"<".$p_params["email_mail_ori"].">".$passage_ligne;
            }
            if(($p_params["email_mail_reply"] != null)&&($p_params["email_labelle_reply"] != null)){
                $header.= "Reply-to: \"".$p_params["email_labelle_reply"]."\" <".$p_params["email_mail_reply"].">".$passage_ligne;
            }
            if($p_params["email_mails_copy"] != null){
                $header.= "Cc: ".$p_params["email_mails_copy"]."".$passage_ligne;
            }
            if($p_params["email_mails_cache"] != null){
                $header.= "Bcc: ".$p_params["email_mails_cache"]."".$passage_ligne;
            }
            $header.= "MIME-Version: 1.0".$passage_ligne;
            $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
            //==========

            //=====Création du message.
            $message = $passage_ligne."--".$boundary.$passage_ligne;
            //=====Ajout du message au format texte.
            $message.= "Content-Type: text/plain; charset=\"utf-8\"".$passage_ligne;
            $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
            $message.= $passage_ligne.$p_params["message_txt"].$passage_ligne;
            //==========
            $message.= $passage_ligne."--".$boundary.$passage_ligne;
            //=====Ajout du message au format HTML
            $message.= "Content-Type: text/html; charset=\"utf-8\"".$passage_ligne;
            $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
            $message.= $passage_ligne.$p_params["message_html"].$passage_ligne;
            //==========
            $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
            $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
            //==========

            //=====Envoi de l'e-mail.
            $preferences = ['input-charset' => 'UTF-8', 'output-charset' => 'UTF-8'];
            $encoded_subject = iconv_mime_encode('Subject', $p_params["sujet"], $preferences);
            $encoded_subject = substr($encoded_subject, strlen('Subject: '));
            if(!mail($p_params["email_mails_dest"],$encoded_subject,$message,$header)){
              $strRetour = "KO";
            }
            //==========
            gotoFinish:
            return $strRetour;
        } catch (Exception $e) {
            $msg = "Function: " . __FUNCTION__ . ", error: " . $e->getMessage();
            Throw new Exception($msg);
            return null;
       }
    }

    /**
     * @author Fabrice Rosito <rosito.fabrice@atbq.fr>
     * @param stdClass $params
     * @param String $params->from
     * @param String $params->fromLabel
     * @param String $params->to
     * @param String $params->subject
     * @param String $params->bodyText
     * @param String $params->bodyHtml
     * @param String $params->reply
     * @param String $params->replyLabel
     * @param String $params->cc
     * @param String $params->cci
     * @version 180221
     */
    static function mail($params){
        try {
            $config = OdaConfig::getInstance();

            if($config->MAILGUN->isOK()){
                $paramsMailGun = array();
                $paramsMailGun["email_mails_dest"] = $params->to;
                $paramsMailGun["message_html"] = $params->bodyHtml;
                $paramsMailGun["sujet"] = $params->subject;
                $paramsMailGun["email_mail_ori"] = $params->from;
                $paramsMailGun["email_labelle_ori"] = $params->fromLabel;
                $paramsMailGun["email_mail_reply"] = $params->reply;
                $paramsMailGun["email_labelle_reply"] = $params->replyLabel;
                $paramsMailGun["email_mails_copy"] = $params->cc;
                $paramsMailGun["email_mails_cache"] = $params->cci;
                $paramsMailGun["message_txt"] = $params->bodyText;
                $result = self::sendMailGun($paramsMailGun);
            }elseif (isset($config->AWS_SES)){
                $result = self::sendMailSES($params);
                if($result){
                    $result = $result->get('MessageId');
                }
            }else{
                $paramsMail = array();
                $paramsMail["email_mails_dest"] = $params->to;
                $paramsMail["message_html"] = $params->bodyHtml;
                $paramsMail["sujet"] = $params->subject;
                $paramsMail["email_mail_ori"] = $params->from;
                $paramsMail["email_labelle_ori"] = (isset($params->fromLabel))?$params->fromLabel:null;
                $paramsMail["email_mail_reply"] = (isset($params->reply))?$params->reply:null;
                $paramsMail["email_labelle_reply"] = (isset($params->replyLabel))?$params->replyLabel:null;
                $paramsMail["email_mails_copy"] = (isset($params->cc))?$params->cc:null;
                $paramsMail["email_mails_cache"] = (isset($params->cci))?$params->cci:null;
                $paramsMail["message_txt"] = (isset($params->bodyText))?$params->bodyText:null;
                $result = self::sendMail($paramsMail);
            }

            return $result;
        } catch (Exception $e) {
            $msg = "Function: " . __FUNCTION__ . ", error: " . $e->getMessage();
            Throw new Exception($msg);
            return null;
        }
    }

    /**
     * get_string_between
     * @param string $string
     * @param string $start
     * @param string $end
     * @return string
     * @throws Exception
     */
    static function get_string_between($string, $start, $end){
        try {
            $string = " ".$string;
            $ini = strpos($string,$start);
            if ($ini == 0) return "Start non trouve";
            $ini += strlen($start);
            $len = strpos($string,$end,$ini) - $ini;
            return substr($string,$ini,$len);
        } catch (Exception $e) {
            $msg = $e->getMessage();
            Throw new Exception('Erreur dans '.__CLASS__.' : '.$msg);
            return null;
        }
    }
        
    /**
     * class CallRest
     * @access public
     * @param type $method : POST, PUT, GET etc
     * @param type $url
     * @param type $data : array("param" => "value") ==> index.php?param=value
     * @return type
     */
    static function CallRest($p_url, $p_params = null, $p_data = null){
        $result = null;
        $url = $p_url;

        $params_attempt = new stdClass();
        $params_attempt->method = 'GET';
        $params_attempt->dataTypeRest = 'json';
        $params_attempt->debug = false;

        try {
            $params = (object) array_merge((array) $params_attempt, (array) $p_params);

            if($params->debug){
                echo('$p_url :');
                var_dump($p_url);
                echo('$params :');
                var_dump($params);
                echo('$p_data :');
                var_dump($p_data);
            }

            $curl = curl_init();

            switch ($params->method)
            {
                case "POST":
                    curl_setopt($curl, CURLOPT_POST, 1);

                    if ($p_data != null)
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $p_data);
                    break;
                case "PUT":
                    curl_setopt($curl, CURLOPT_PUT, 1);
                    break;
                default:
                    if ($p_data != null)
                        $url = sprintf("%s?%s", $url, http_build_query($p_data));
            }

            // Optional Authentication:
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, "username:password");

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

            $retourCall = curl_exec($curl);

            curl_close($curl);

            switch ($params->dataTypeRest)
            {
                case "json":
                    $result = json_decode($retourCall);
                    break;
                default:
                    $result = $retourCall;
            }

            if($result == null){
                $result = $retourCall;
            }

            if($params->debug){
                echo('$result :');
                var_dump($result);
            }

            return $result;
        } catch (Exception $e) {
            $msg = $e->getMessage();
            Throw new Exception('Erreur dans '.__CLASS__.' : '.$msg);
            return null;
        }
    }
    static function startsWith($haystack, $needle) {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
    }
    static function endsWith($haystack, $needle) {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
    }

    static function recurse_copy($src,$dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    OdaLib::recurse_copy($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    copy($src . '/' . $file,$dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    /**
     * @name var_debug
     * @param Variable
     */
    static function var_debug($variable,$strlen=100,$width=25,$depth=10,$i=0,&$objects = array()){
        $search = array("\0", "\a", "\b", "\f", "\n", "\r", "\t", "\v");
        $replace = array('\0', '\a', '\b', '\f', '\n', '\r', '\t', '\v');
        
        $string = '';
        
        switch(gettype($variable)) {
            case 'boolean':      $string.= $variable?'true':'false'; break;
            case 'integer':      $string.= $variable;                break;
            case 'double':       $string.= $variable;                break;
            case 'resource':     $string.= '[resource]';             break;
            case 'NULL':         $string.= "null";                   break;
            case 'unknown type': $string.= '???';                    break;
            case 'string':
            $len = strlen($variable);
            $variable = str_replace($search,$replace,substr($variable,0,$strlen),$count);
            $variable = substr($variable,0,$strlen);
            if ($len<$strlen) $string.= '"'.$variable.'"';
            else $string.= 'string('.$len.'): "'.$variable.'"...';
            break;
            case 'array':
            $len = count($variable);
            if ($i==$depth) $string.= 'array('.$len.') {...}';
            elseif(!$len) $string.= 'array(0) {}';
            else {
                $keys = array_keys($variable);
                $spaces = str_repeat(' ',$i*2);
                $string.= "array($len)\n".$spaces.'{';
                $count=0;
                foreach($keys as $key) {
                if ($count==$width) {
                    $string.= "\n".$spaces."  ...";
                    break;
                }
                $string.= "\n".$spaces."  [$key] => ";
                $string.= self::var_debug($variable[$key],$strlen,$width,$depth,$i+1,$objects);
                $count++;
                }
                $string.="\n".$spaces.'}';
            }
            break;
            case 'object':
            $id = array_search($variable,$objects,true);
            if ($id!==false)
                $string.=get_class($variable).'#'.($id+1).' {...}';
            else if($i==$depth)
                $string.=get_class($variable).' {...}';
            else {
                $id = array_push($objects,$variable);
                $array = (array)$variable;
                $spaces = str_repeat(' ',$i*2);
                $string.= get_class($variable)."#$id\n".$spaces.'{';
                $properties = array_keys($array);
                foreach($properties as $property) {
                $name = str_replace("\0",':',trim($property));
                $string.= "\n".$spaces."  [$name] => ";
                $string.= self::var_debug($array[$property],$strlen,$width,$depth,$i+1,$objects);
                }
                $string.= "\n".$spaces.'}';
            }
            break;
        }
        
        if ($i>0) return $string;
        
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        do $caller = array_shift($backtrace); while ($caller && !isset($caller['file']));
        if ($caller) $string = $caller['file'].':'.$caller['line']."\n".$string;
        $string .= "\n";
        
        echo nl2br(str_replace(' ','&nbsp;',htmlentities($string)));
    }

    /**
     * @name var_debug
     * @param Variable
     */
    static function traceLog($errstr, $errno = E_USER_NOTICE, $echo = true){
        if(!ini_get('date.timezone')){
            date_default_timezone_set('GMT');
        }

        $logfile = './log_'.date("y-m-d").'.log';

        $msg;

        switch ($errno) {
            case E_USER_ERROR:
                $msg = date("y-m-d H:i:s") . " >> ".$_SERVER['PHP_SELF']." >> ERROR >> [$errno] $errstr". PHP_EOL;
                break;

            case E_USER_WARNING:
                $msg = date("y-m-d H:i:s") . " >> ".$_SERVER['PHP_SELF']." >> WARNING >> $errstr". PHP_EOL;
                break;

            case E_USER_NOTICE:
                $msg = date("y-m-d H:i:s") . " >> ".$_SERVER['PHP_SELF']." >> NOTICE >> $errstr". PHP_EOL;
                break;

            default:
                $msg = date("y-m-d H:i:s") . " >> ".$_SERVER['PHP_SELF']." >> UNKNOWN >> $errstr". PHP_EOL;
                break;
        }

        if($echo){
            echo $msg;
        }
        file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
    }

    /**
     * @return Boolean
     */
    static function isInteger($input){
        try{
            return(ctype_digit(strval($input)));
        } catch (Exception $e) {
            Throw new Exception("Class: " . __CLASS__ . " Function: " . __FUNCTION__ . ", error: " . $e->getMessage());
            return null;
        }
    }

    function Zip($source, $destination){
        try{
            if (!extension_loaded('zip') || !file_exists($source)) {
                Throw new Exception("Class: " . __CLASS__ . " Function: " . __FUNCTION__ . ", error: Extention zip is missing");
            }

            $zip = new \ZipArchive();
            if (!$zip->open($destination, \ZIPARCHIVE::CREATE)) {
                return false;
            }

            $source = str_replace('\\', '/', realpath($source));

            if (is_dir($source) === true)
            {
                $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($source), \RecursiveIteratorIterator::SELF_FIRST);

                foreach ($files as $file)
                {
                    $file = str_replace('\\', '/', $file);

                    // Ignore "." and ".." folders
                    if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                        continue;

                    $file = realpath($file);

                    if (is_dir($file) === true)
                    {
                        $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                    }
                    else if (is_file($file) === true)
                    {
                        $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                    }
                }
            }
            else if (is_file($source) === true)
            {
                $zip->addFromString(basename($source), file_get_contents($source));
            }

            return $zip->close();
        } catch (Exception $e) {
            Throw new Exception("Class: " . __CLASS__ . " Function: " . __FUNCTION__ . ", error: " . $e->getMessage());
            return null;
        }
    }

    /**
    * @name uncryptODA
    * @param data
    * @return Boolean
    */
    static function isTrue($val) {
        try {
            if(is_bool($val)){
                if($val === true){
                    return true;
                }else{
                    return false;
                }
            }

            if(is_string($val)){
                if($val === "1" || $val ===  "true"){
                    return true;
                }else{
                    return false;
                }
            }

            if(is_int($val)){
                if($val === 1){
                    return true;
                }else{
                    return false;
                }
            }

            return false;
        } catch (Exception $e) {
            $msg = $e->getMessage();
            Throw new Exception('Erreur dans '.__CLASS__.' : '.$msg);
            return null;
        }
    }
}